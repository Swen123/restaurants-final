import { Controller } from 'stimulus';
import axios from 'axios'


export default class extends Controller {

    connect(){

		let submitForm = document.getElementById('submitFermeture')
		let msg = document.getElementById('messageFermeture')

		submitForm.addEventListener('click', (event) => {
			event.preventDefault()

			let dateDebut = document.getElementById('deb_conge')
			let dateFin = document.getElementById('fin_conge')

			axios.post("/conge/add", null, {
				params: {
					deb_conge: dateDebut.value,
					fin_conge: dateFin.value
				}
			})
			.then( res => {

				document.getElementById('deb_conge').value = ""
				document.getElementById('fin_conge').value = ""

				msg.innerHTML = ""
				msg.innerHTML = `
					<div class="mt-5 alert alert-${res.data.type}" role="alert">
						${res.data.message}
					</div>
				`

				this.loadFermeture()
			})
			.catch(err => {

				msg.innerHTML = ""
				msg.innerHTML = `
					<div class="mt-5 alert alert-danger" role="alert">
						${err}
					</div>
				`
			})
		})



		this.loadFermeture()
    }

    loadFermeture() {
		axios.get("/find/fermeture")
			.then((res) => {
				let jsonLength = Object.keys(res.data).length
				let table = document.getElementById('fermetureTab')
				let content = ""

				//vide le tableau
				table.innerHTML = ""


				//remplissage du tableau
				for (let i = 0; i < jsonLength; i++) {

					content += `
                        <tr>           
                            <td>${res.data[0].dateDebut}</td>
                            <td>${res.data[0].dateFin}</td>
                            <td><button id="deleteFermeture" type="button" value="${res.data[0].id}" class="btn btn-danger">Supprimer</button></td>
                        </tr>
                            `
				}

				table.innerHTML = content
				this.deleteFermeture()
			})
	}


	deleteFermeture(){
		let btn = document.querySelectorAll('#deleteFermeture')

		btn.forEach( e => {
			e.addEventListener('click', (event) => {

				let id = event.target.value
				let msg = document.getElementById('messageFermeture')

				axios.post("/conge/delete/" + id)
					.then(res => {
						msg.innerHTML = ""
						msg.innerHTML = `
							<div class="mt-5 alert alert-${res.data.type}" role="alert">
								${res.data.message}
							</div>
						`
						this.loadFermeture()
					})
					.catch(err => {

						msg.innerHTML = ""
						msg.innerHTML = `
							<div class="mt-5 alert alert-danger" role="alert">
								${err}
							</div>
						`
					})
			})
		})
	}
}