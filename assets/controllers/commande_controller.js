import { Controller } from 'stimulus';
import Cookies from 'js-cookie'
import axios from 'axios'

export default class extends Controller {

    connect() {
        
        let date = document.getElementById('commande_date')
        let containerDate = document.getElementById('horaire-form-commande')

        let service = document.getElementById('service-form-commande')
        let services = document.querySelectorAll('#commande_service')

        let next = false


        Cookies.remove('commande_date')
        Cookies.remove('commande_service')
        Cookies.remove('commande_horaire')


        date.addEventListener('change', () => {
            const dateValue = date.value

            if (Cookies.get('commande_service') != null && Cookies.get('commande_date') != null) {
                
                Cookies.remove('commande_date')
                Cookies.set('commande_date', dateValue)
                this.horaireForm(false, Cookies.get('commande_service'))
            }

            if (dateValue !== "") {
                document.getElementById('message-js-commande').innerHTML = ""
                service.style.display = "block"
                next = true
                Cookies.remove('commande_date')
                Cookies.set('commande_date', dateValue)
            } else {
                document.getElementById('message-js-commande').innerHTML = `<p class="mt-5 alert alert-danger">Vous devez saisir une date</p>`
            }

            if (next) {
                services.forEach(e => {
                    e.addEventListener('click', (input) => {
                        containerDate.style.display = "block"
                        document.getElementById('horaire-commande-container-matin').innerHTML = ""
                        document.getElementById('horaire-commande-container-soir').innerHTML = ""
                        Cookies.set('commande_service', input.target.value)

                        this.horaireForm(false,  Cookies.get('commande_service'))
                    })
                })
            }




        })


        // document.getElementById('commander').addEventListener('click', (event) => {
        //     event.preventDefault()
        
        //     axios.post("/new/commande/" + id_resto.value, null, { params: {
        //         date : Cookies.get('commande_date'),
        //         service : Cookies.get('commande_service'),
        //         horaire : Cookies.get('commande_horaire')
        //       }})
        //       .then(response => {
        
        //         document.getElementById('message-js-commande').innerHTML = ""
        //         document.getElementById('message-js-commande').innerHTML = `<p class="mt-5 alert alert-${response.data.type}">${response.data.message}</p>`
                
        //       if(response.data.type == "success"){
        //         Cookies.remove('commande_date')
        //         Cookies.remove('commande_service')
        //         Cookies.remove('commande_horaire')
        //       }
        //     })
        //     .catch(err => console.warn(err));
        
        // })

    }

    horaireForm(serviceEmpty) {

        const id_resto = parseInt(document.getElementById('id_resto').value)
        let horaire = document.getElementById('horaire-form-commande')
        let containerHoraireM = document.getElementById('horaire-commande-container-matin')
        let containerHoraireS = document.getElementById('horaire-commande-container-soir')

        containerHoraireM.innerHTML = ""
        containerHoraireS.innerHTML = ""

        if (!serviceEmpty) {
            horaire.style.display = "block"

            axios.post("/horaire/commande/" + id_resto, null, {
                    params: {
                        dateC: Cookies.get('commande_date')
                    }
                })
                .then((result) => {
                    document.getElementById('message-js-commande').innerHTML = ""
                    let horairesM = result.data.horaireM
                    let horairesS = result.data.horaireS


                    let horaireListM = []
                    let horaireListS = []


                    if (Object.keys(horairesM).length > 0 || Object.keys(horairesS).length > 0) {

                        for (let i = 1; i < Object.keys(horairesM).length + 1; i++) {
                            horaireListM += `
                            <label id="label-js" class="radio-h">${horairesM[i]}
                                <input type="radio" id="horaire-radio" id="horaire-js" name="commande_horaire" value="${horairesM[i]}">
                            </label>
                        `
                        }

                        for (let i = 1; i < Object.keys(horairesS).length + 1; i++) {
                            horaireListS += `
                            <label id="label-js" class="radio-h">${horairesS[i]}
                                <input type="radio" id="horaire-radio" id="horaire-js" name="commande_horaire" value="${horairesS[i]}">
                            </label>
                        `
                        }

                        containerHoraireM.innerHTML = horaireListM
                        containerHoraireS.innerHTML = horaireListS

                        this.horaireBtn()

                    } else {
                        containerHoraireM.innerHTML = `<p class="alert alert-danger">${result.data.message}</p>`
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
        }
    }

    horaireBtn() {
        document.querySelectorAll("#label-js").forEach(input => {

            input.addEventListener('click', (e) => {

                if (e.target.childNodes[1] != undefined) {

                    Cookies.set('commande_horaire', e.target.childNodes[1].value)

                    //document.getElementById('commander').style.display = 'block'
                    this.listeProduit()
                }
            })
        })
    }


    listeProduit(){
        const id_resto = parseInt(document.getElementById('id_resto').value)
        let divMenu = document.getElementById("menu")
            let divCarte = document.getElementById("carte")

            axios.get("/get/product/" + id_resto)
                .then( res => {
                    let menu = res.data.categoriesMenu
                    let carte = res.data.categoriesCarte

                    let jsonLengthMenu = Object.keys(menu).length
                    let jsonLengthCarte = Object.keys(carte).length

                    let contentMenu = ""
                    let contentCarte = ""
    
                    //vide le tableau
                    divMenu.innerHTML = ""
                    divCarte.innerHTML = ""
        
                    //remplissage du tableau menu
                    for (let i = 0; i < jsonLengthMenu; i++) {
    
                        contentMenu += `
                            <div class="accordion-item mt-3">
                                <h2 class="accordion-header" id="heading${i}">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse${i}" aria-expanded="true" aria-controls="collapse${i}">
                                    ${menu[i].libelle}
                                </button>
                                </h2>
                                <div id="collapse${i}" class="accordion-collapse collapse" aria-labelledby="heading${i}">
                                    <div class="accordion-body" id="contentProductMenu${menu[i].id}">
       
                                    </div>
                                </div>
                            </div>
                        `
                        this.productByCatMenu(menu[i].id)
                    }
                    divMenu.innerHTML = contentMenu


                    //remplissage du tableau Carte
                    for (let x = 0; x < jsonLengthCarte; x++) {
    
                        contentCarte += `
                            <div class="accordion-item mt-3">
                                <h2 class="accordion-header" id="heading-carte${x}">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#carte${x}" aria-expanded="true" aria-controls="carte${x}">
                                    ${carte[x].libelle}
                                </button>
                                </h2>
                                <div id="carte${x}" class="accordion-collapse collapse" aria-labelledby="heading-carte${x}">
                                    <div class="accordion-body" id="contentProductCarte${carte[x].id}">
                                        
                                    </div>
                                </div>
                            </div>
                        `
                        this.productByCat(carte[x].id)
                    }
    
                    divCarte.innerHTML = contentCarte
                })


              
    }

    productByCat(id){
        const idCateg = id

        axios.get("/get/product/cat/" + id)
            .then((res) => {
                let content = document.getElementById('contentProductCarte' + idCateg)
                let table = ""

                for(let i = 0; i < Object.keys(res.data.produits).length; i++){
                    table += `
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault" >
                                ${res.data.produits[i].nom}               
                            </label>
                            <span><button class="btn btn-sq-xs btn-primary" id="more">+</button><span id="qte">1</span><button class="btn btn-sq-xs btn-primary" id="less">-</button><span id="prixProduit">${res.data.produits[i].prix}</span>  
                        </div>
                    `
                }

                content.innerHTML = table
                this.loadBtnMoreLess()
            })
    }


    productByCatMenu(id){
        const idCateg = id

        axios.get("/get/product/cat/" + id)
            .then((res) => {
                let content = document.getElementById('contentProductMenu' + idCateg)
                let table = ""

                for(let i = 0; i < Object.keys(res.data.produits).length; i++){
                    table += `
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault" >
                            <span id="nomProduit">${res.data.produits[i].nom}                
                            </label>
                            <span><button class="btn btn-sq-xs btn-primary" id="more">+</button><span id="qte">1</span><button class="btn btn-sq-xs btn-primary" id="less">-</button><span id="prixProduit">${res.data.produits[i].prix} €</span> 
                        </div>
                    `
                }

                content.innerHTML = table
                this.loadBtnMoreLess()
            })
    }


    choixCommande(){
        let checkbox = document.querySelectorAll('#flexCheckDefault')

        checkbox.forEach(input => {




            input.addEventListener('change', (event) => {

            })
        })
    }


    loadBtnMoreLess(){
            let minusBtn = document.querySelectorAll("#less"),
                plusBtn = document.querySelectorAll("#more"),
                numberPlace = document.getElementById("qte"),
                number = 0, /// number value
                min = 0, /// min number
                max = 30; /// max number
            
        minusBtn.forEach(e => {
            e.addEventListener('click', (event) => {

                if (number>min){
                    number = number-1
                    event.target.previousSibling.innerText = number
                    
                }  
            })
        })

        plusBtn.forEach(e => {
            e.addEventListener('click', (event) => {
                if(number<max){
                    number = number+1
                    event.target.nextSibling.innerText = number
                }        
            })
        })
    }
}
