import { Controller } from 'stimulus';
import axios from 'axios'

export default class extends Controller {

    connect() {

        var acc = document.getElementsByClassName("accordion");
        var i;
        
        for (i = 0; i < acc.length; i++) {
          acc[i].addEventListener("click", function() {
            this.classList.toggle("active-accor");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            } 
          });
        }









        const roleUser = document.getElementById('role-user').value

        //Modification des options [RESTAURATEUR] 
        // - Accepter les commandes en ligne
        // - Accepter les reservations en ligne
        // - Envoi automatique de SMS


        // let commande = document.getElementById('commande')
        // let reservation = document.getElementById('reservation')
        // let sms = document.getElementById('sms')
        if(roleUser === "ROLE_RESTAURANT"){
            let optionCheck = document.querySelectorAll('#option-check')

            optionCheck.forEach(element => {
                element.addEventListener('click', (event) => {
                    if(event.target.type === "checkbox"){
    
                        let commandeCheck = parseInt(document.getElementById('commande').value)
                        let reservationCheck = parseInt(document.getElementById('reservation').value)
                        let smsCheck = parseInt(document.getElementById('sms').value)
    
                        if(event.target.id == "reservation"){
                            reservationCheck = event.target.checked === true ? 1 : 0
                        }
    
                        if(event.target.id == "commande"){
                            commandeCheck = event.target.checked === true ? 1 : 0
                        }
    
                        if(event.target.id == "sms"){
                            smsCheck = event.target.checked === true ? 1 : 0
                        }
    
                        axios.post("/set/restaurant/options",null, { params: {
                            commande : commandeCheck,
                            reservation : reservationCheck,
                            sms : smsCheck
                        }})
                        .then((result) => {
                            document.getElementById('commande').value = result.data[0].commandeEnLigne === true ? 1 : 0
                            document.getElementById('reservation').value = result.data[0].reservationEnLigne === true ? 1 : 0
                            document.getElementById('sms').value = result.data[0].envoiSMS === true ? 1 : 0
                        })
                        .catch( (err) => {
                            console.log(err)
                        })
                    }
                    
                })
            })
    
    
            //Formulaire ajout menu et produit
            document.querySelector('#categories-menu').addEventListener('submit', (event) => {
                event.preventDefault()
                
                let nom = document.getElementById('categories_menu_nom').value
                let type = "menu"
    
                axios.post("/ajouter/categorie", null, { params: {
                    nom : nom,
                    type : type
                }})
                .then( (response) => {
                    let msgContainer = document.getElementById('menu-msg')
                    let select = document.getElementById('categorie-menu')
    
                    let newOpt = document.createElement('option')
                    newOpt.value = response.data.categorie_id
                    newOpt.innerHTML = response.data.categorie_nom
                    select.appendChild(newOpt);
    
                    msgContainer.innerHTML = ""
                    msgContainer.innerHTML = `<div class="alert alert-${response.data.msgtype}">${response.data.message}</div>`
                })
            })
    
            document.querySelector('#categories-menu-produit').addEventListener('submit', (event) => {
                event.preventDefault()
                
                let nom = document.getElementById('produit_menu_nom').value
                let prix = document.getElementById('produit_menu_prix').value
                let categorie = document.getElementById('categorie-menu').value
                
        
                axios.post("/ajouter/produit", null, { params: {
                    nom : nom,
                    prix : prix,
                    categorie : categorie
                }})
                .then( (response) => {
                    let msgContainer = document.getElementById('menu-msg')
        
                    msgContainer.innerHTML = ""
                    msgContainer.innerHTML = `<div class="alert alert-${response.data.msgtype}">${response.data.message}</div>`
                })
            })
    
    
            //Formulaire ajout carte et produit
            document.querySelector('#categories-carte').addEventListener('submit', (event) => {
                event.preventDefault()
                
                let nom = document.getElementById('categories_carte_nom').value
                let type = "carte"
        
                axios.post("/ajouter/categorie", null, { params: {
                    nom : nom,
                    type : type
                }})
                .then( (response) => {
                    let msgContainer = document.getElementById('carte-msg')
                    let select = document.getElementById('categorie-carte')
        
                    let newOpt = document.createElement('option')
                    newOpt.value = response.data.categorie_id
                    newOpt.innerHTML = response.data.categorie_nom
                    select.appendChild(newOpt);
        
                    msgContainer.innerHTML = ""
                    msgContainer.innerHTML = `<div class="alert alert-${response.data.msgtype}">${response.data.message}</div>`
                })
            })
        
        
        
            document.querySelector('#categories-carte-produit').addEventListener('submit', (event) => {
                event.preventDefault()
                
                let nom = document.getElementById('produit_carte_nom').value
                let prix = document.getElementById('produit_carte_prix').value
                let categorie = document.getElementById('categorie-carte').value
                
        
                axios.post("/ajouter/produit", null, { params: {
                    nom : nom,
                    prix : prix,
                    categorie : categorie
                }})
                .then( (response) => {
                    let msgContainer = document.getElementById('carte-msg')
        
                    msgContainer.innerHTML = ""
                    msgContainer.innerHTML = `<div class="alert alert-${response.data.msgtype}">${response.data.message}</div>`
                })
            })
        }

        if(roleUser === "ROLE_CLIENT"){
            // Profile : Liste des réservations
            //Fonction d'annulation d'une réservations
            let cancelBTN = document.querySelectorAll('#cancel-reservation')


            cancelBTN.forEach(element => {
                element.addEventListener('click', (event) => {
                    event.preventDefault()
                
                    let url = event.target.href

                    axios.post(url)
                        .then( result => {
                            let tbody = document.getElementById('tbody-reservation')
                            let tabmsg = document.getElementById('tab-message')

                            if(Object.keys(result.data).length > 0){
                                tbody.innerHTML = ""
                                let reservationList = ""
                
                                for(let i = 0; i < Object.keys(result.data).length; i++){
                                    
                                    reservationList += `
                                        <tr>  
                                            <th>${ result.data[i].date}</th>
                                            <td>${ result.data[i].horaire }</td>
                                            <td>${ result.data[i].service }</td>
                                            <td>${ result.data[i].nom }</td>
                                            <td>${ result.data[i].nombre }</td>
                                            <td><a href="/cancel/reservation/${result.data[i].id}/${result.data[i].uuid}" id="cancel-reservation">Annuler</a></td>
                                        </tr>
                                    `
                                }
                
                                tbody.innerHTML = reservationList
                                tabmsg.innerHTML = `<div class="mt-3 alert alert-success" role="alert">La réservation à bien été annulée.</div>`

                            } else {
                                tabmsg.innerHTML = `<div class="mt-3 alert alert-danger" role="alert">Vous ne pouvez pas annuler car vous n'avez aucune réservation.</div>`
                            }
                            
                            
                        })
                        .catch( err => {
                            tabmsg.innerHTML = `<div class="mt-3 alert alert-danger" role="alert">${err}</div>`
                        })
                })
            })
        }

        
        //SMS
        document.getElementById('v-pills-SMS-tab').addEventListener('click', () =>{
            axios.get("/sms/restant")
            .then( res => {
                let restant = parseInt(res.data[0].solde) - parseInt(res.data[0].smsUtilise)
                this.smsAlert(restant)
            })
        })

        //Actualisation du solde des SMS

        let reloadSMS = document.getElementById('reloadSMS')

        reloadSMS.addEventListener('click', (event) => {
            event.preventDefault()
            let url = event.target.href

            axios.get(url).then((response) => {

                document.getElementById('solde').innerHTML = response.data[0].solde
                document.getElementById('smsUtilise').innerHTML = response.data[0].smsUtilise

                let restant = parseInt(response.data[0].solde) - parseInt(response.data[0].smsUtilise)

                document.getElementById('solde').innerHTML = restant
                document.getElementById('restant').innerHTML = restant

                this.smsAlert(restant)
            })
            .catch((err) => {
                console.log(err)
            })
        })
    }


    smsAlert(restant){

        if(restant <= 50){

            document.getElementById('msg-sms').innerHTML = ""

            document.getElementById('msg-sms').innerHTML = `
                <div class="mt-5 alert alert-danger" role="alert">
                    Il ne vous reste que quelques SMS, vous pouvez les acheter dès maintenant 
                </div>`

            this.sleep(4000).then(() => {
                document.getElementById('msg-sms').innerHTML = ""
            });
        }
    }

    sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}
}

