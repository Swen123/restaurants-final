import { Controller } from 'stimulus';
import Cookies from 'js-cookie'
import axios from 'axios'


export default class extends Controller {


    

    connect() {

        const id_resto = parseInt(document.getElementById('id_resto').value)
        let date = document.getElementById('reservation_date')
        let service = document.getElementById('service-form')
        let services = document.querySelectorAll('#service')

        let next = false
        

        Cookies.remove('date')
        Cookies.remove('service')
        Cookies.remove('horaire')
        Cookies.remove('couvert')


        date.addEventListener('change', () => {
            const dateValue = date.value

            if (Cookies.get('service') != null && Cookies.get('date') != null) {
                this.horaireForm(false, Cookies.get('service'), dateValue)
            }



            if (dateValue !== "") {
                document.getElementById('message-js').innerHTML = ""
                service.style.display = "block"
                next = true
                Cookies.remove('date')
                Cookies.set('date', dateValue)
            } else {
                document.getElementById('message-js').innerHTML = `<p class="mt-5 alert alert-danger">Vous devez saisir une date</p>`
            }

            if (next) {
                services.forEach(e => {
                    e.addEventListener('click', (input) => {
                        document.getElementById('radio-container').innerHTML = ""
                        let serviceValue = input.target.value
                        Cookies.set('service', serviceValue)
                        this.horaireForm(false, serviceValue, dateValue)
                    })
                })
            }
        })




        document.getElementById('test').addEventListener('click', (event) => {
            event.preventDefault()

            axios.post("/new/reservation/" + id_resto, null, {
                    params: {
                        date: Cookies.get('date'),
                        service: Cookies.get('service'),
                        horaire: Cookies.get('horaire'),
                        couvert: Cookies.get('couvert')
                    }
                })
                .then(response => {

                    document.getElementById('message-js').innerHTML = ""
                    document.getElementById('message-js').innerHTML = `<p class="mt-5 alert alert-${response.data.type}">${response.data.message}</p>`

                    if (response.data.type == "success") {
                        Cookies.remove('date')
                        Cookies.remove('service')
                        Cookies.remove('horaire')
                        Cookies.remove('couvert')
                    }
                })
                .catch(err => console.warn(err));
        })
    }

    horaireBtn() {
        let couvert = document.getElementById('couvert-form')
        document.querySelectorAll("#label-js").forEach(input => {

            input.addEventListener('click', (e) => {

                if (e.target.childNodes[1] != undefined) {

                    Cookies.set('horaire', e.target.childNodes[1].value)
                    couvert.style.display = "block"
                    let couvertValue = document.getElementById('couvert')

                    couvertValue.addEventListener('focusout', () => {

                        if (couvertValue.value != "") {
                            document.getElementById('message-js').innerHTML = ""
                            //Les données peuvent être validé
                            Cookies.set('couvert', parseInt(couvertValue.value))
                        } else {
                            document.getElementById('message-js').innerHTML = `<p class="mt-5 alert alert-danger">Vous devez saisir un nombre de couvert(s)</p>`
                        }

                    })
                }
            })
        })
    }

    horaireForm(serviceEmpty, serviceValue, dateValue) {

        const id_resto = parseInt(document.getElementById('id_resto').value)
        let horaire = document.getElementById('horaire-form')
        let containerHoraire = document.getElementById('radio-container')
        containerHoraire.innerHTML = ""

        if (!serviceEmpty) {
            horaire.style.display = "block"



            axios.post("/get/horaire/" + serviceValue + "/" + id_resto + "/" + dateValue)
                .then((result) => {
                    document.getElementById('message-js').innerHTML = ""
                    let horaires = result.data.horaire
                    let count = Object.keys(horaires).length
                    let horaireList = []


                    if (count > 0) {
                        for (let i = 1; i < count + 1; i++) {
                            horaireList += `
                            <label id="label-js" class="radio-h">${horaires[i]}
                                <input type="radio" id="horaire-radio" id="horaire-js" name="horaire" value="${horaires[i]}">
                            </label>
                        `
                        }

                        containerHoraire.innerHTML = horaireList

                        this.horaireBtn()

                    } else {
                        containerHoraire.innerHTML = `<p class="alert alert-danger">${result.data.message}</p>`
                    }
                })
                .catch((err) => {
                    console.log(err)
                })
        }
    }
}