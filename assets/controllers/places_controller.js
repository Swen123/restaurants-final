import { Controller } from 'stimulus';
import axios from 'axios'

export default class extends Controller {

	connect() {

		//affichage des lignes dans le tableau
		this.loadPlace()


		//Ajout
		document.getElementById('submitPlaces').addEventListener('click', (event) => {
			event.preventDefault()

			let dateInput = document.getElementById('date_place').value
			let placesInput = parseInt(document.getElementById('places').value)
			let id = parseInt(document.getElementById('idResto-js').value)
			let msgContainer = document.getElementById('places-msg')
			let date = new Date();

			if (Date.parse(dateInput) >= date) {

				axios.post("/add/places", null, {
						params: {
							date: dateInput,
							places: placesInput,
							id: id,
						}
					})
					.then((res) => {


						this.loadPlace()
						msgContainer.innerHTML = ""
						msgContainer.innerHTML = `<div class="mt-5 alert alert-${res.data.type}" role="alert">${res.data.message}</div>`
                        
                        this.sleep(2000).then(() => {
                            msgContainer.innerHTML = ""
                        });

					})
					.catch((err) => {
						msgContainer.innerHTML = ""
						msgContainer.innerHTML = `<div class="mt-5 alert alert-danger" role="alert">${err}</div>`

                        this.sleep(2000).then(() => {
                            msgContainer.innerHTML = ""
                        });
					})
			} else {
				msgContainer.innerHTML = ""
				msgContainer.innerHTML = `<div class="mt-5 alert alert-danger" role="alert">Vous ne pouvez pas saisir une date antérieur.</div>`

                this.sleep(2000).then(() => {
                    msgContainer.innerHTML = ""
                });
			}
		})

	}

	liberees() {
		//Libérées des places
		let btnLiberees = document.querySelectorAll('#libereesPlaces')
		let containerForm = document.getElementById('formliberees')

		btnLiberees.forEach(element => {
			element.addEventListener('click', (event) => {

				containerForm.innerHTML = `
                   
                                   <h5 class="mt-5">Des places ce sont libérées ?</h5>
           
                                   <label for="inputState" class="mt-3 form-label">Place libérées</label>
                                   <input type="number" id="placesLib" name="places" class="form-control" required>
                                   <button type="submit" id="submitLiberees" class="mt-3 btn btn-primary">Valider</button>
                           
                           `
				this.submitLiberees(event.target)
			})
		})
	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}

	submitLiberees(cible) {

		let id = parseInt(cible.value)

		document.getElementById('submitLiberees').addEventListener('click', () => {


			let inputSaisie = parseInt(document.getElementById('placesLib').value)
			let msgContainer = document.getElementById('places-msg')


			axios.get("/find/places/" + id)
				.then((res) => {

					let lib = parseInt(res.data[0].liberees) + parseInt(inputSaisie)
					let dispo = parseInt(res.data[0].reservable) - parseInt(res.data[0].reservees) + parseInt(inputSaisie)
					let reservees = parseInt(res.data[0].reservees) - parseInt(inputSaisie)

					//update
					//if (reservees >= parseInt(document.getElementById('placesLib').value)) {

						document.getElementById('placeLiberees-' + id).innerHTML = ""
						document.getElementById('placeLiberees-' + id).innerHTML = lib.toString()

						document.getElementById('placeDispo-' + id).innerHTML = ""
						document.getElementById('placeDispo-' + id).innerHTML = dispo.toString()

						document.getElementById('placeReservees-' + id).innerHTML = ""
						document.getElementById('placeReservees-' + id).innerHTML = reservees.toString()

						axios.post("/update/places/" + id, null, {
								params: {
									liberees: lib,
									reservees: reservees
								}
							})
							.then((res) => {
								msgContainer.innerHTML = ""
								msgContainer.innerHTML = `<div class="mt-5 alert alert-${res.data.type}" role="alert">${res.data.message}</div>`

								if (res.data.code === 200) {
									document.getElementById('formliberees').innerHTML = ""

									this.sleep(2000).then(() => {
										msgContainer.innerHTML = ""
									});

								}
							})

					//} else {

						msgContainer.innerHTML = ""
						msgContainer.innerHTML = `<div class="mt-5 alert alert-danger" role="alert">Vous ne pouvez pas avoir plus de places disponique que de réservable.</div>`

						this.sleep(2000).then(() => {
							msgContainer.innerHTML = ""
						});
						
					//}
				})
		})
	}

	loadPlace() {
		axios.get("/find/places")
			.then((res) => {
				let jsonLength = Object.keys(res.data).length
				let table = document.getElementById('bodyPlaces')
				let content = ""

				//vide le tableau
				table.innerHTML = ""


				//remplissage du tableau
				for (let i = 0; i < jsonLength; i++) {

					content += `
                                <tr id="row_places">  
                                    <td id="dataPlace-${res.data[i].id}">${res.data[i].date}</td>         
                                    <td id="placeReservables-${res.data[i].id}">${res.data[i].reservable}</td>
                                    <td id="placeLiberees-${res.data[i].id}">${res.data[i].liberees}</td>
                                    <td id="placeDispo-${res.data[i].id}">${res.data[i].reservable - res.data[i].reservees}</td>
									<td id="placeReservees-${res.data[i].id}">${res.data[i].reservees}</td>
                                    <td>
										<button type="button" value="${res.data[i].id}" id="libereesPlaces" class="btn btn-primary">Libérer</button>
										<button type="button" value="${res.data[i].id}" id="actualiserPlaces" class="btn btn-primary">Actualiser</button>
									</td>
                                </tr>
                            `
				}

				table.innerHTML = content
				this.liberees()
				this.ActualisationBtn()
			})
	}

	ActualisationBtn(){
		//Bouton actualiser

		let btnActualiser = document.querySelectorAll('#actualiserPlaces')

		btnActualiser.forEach(btn => {
			btn.addEventListener('click', (event) => {
		
				let id = event.target.value
				
				axios.get("/find/places/" + id)
				.then( res => {


					document.getElementById('placeDispo-' + id).innerHTML = res.data[0].reservable - res.data[0].reservees
					document.getElementById('placeReservees-' + id).innerHTML = res.data[0].reservees

					console.log(res.data[0])
				})
			})
		})
		
	}	
}