import { Controller } from 'stimulus';

export default class extends Controller {

    connect() {

        let restaurantForm = document.getElementById('restaurant-form')
        let ImRestaurant = document.getElementById('restaurant-checkbox')

        ImRestaurant.addEventListener('click', () => {
            if(ImRestaurant.checked){
                document.getElementById('user-form').style.display = "none"
                restaurantForm.innerHTML = `
                <div class="form-group">
                    <input type="text" class="form-control rounded-left" name="nom_resto" placeholder="Nom du restaurant">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control rounded-left" name="adresse" placeholder="Adresse du restaurant">
                </div>
                <div class="form-group">
                    <input type="number" class="form-control rounded-left" name="zip" placeholder="Code postal">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control rounded-left" name="ville" placeholder="Ville">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control rounded-left" name="telephone" placeholder="Telephone">
                </div>
                
                `
            } else {
                restaurantForm.innerHTML = ""
                document.getElementById('user-form').style.display = "block"
            }
        })
    }

}

