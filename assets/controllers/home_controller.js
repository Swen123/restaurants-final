import { Controller } from 'stimulus';
import axios from 'axios'

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */

//Input choix de la ville
const where = document.getElementById('where')
const display_choice_where = document.getElementById('display-choice-where')

//Input choix de la spécialité ou du nom du restaurant
const what = document.getElementById('what')
const display_choice_what = document.getElementById('display-choice-what')

export default class extends Controller {

    connect() {

        where.addEventListener('focus', () => {
            where.addEventListener('input', (event) => {

                let search = event.target.value

                if (search.length > 0) {
                    axios.get("/search/city=" + search)
                        .then((result) => {

                            if (Object.keys(result.data.result).length > 0) {
                                display_choice_where.innerHTML = ''

                                let list = ""

                                for (let i = 0; i < Object.keys(result.data.result).length; i++) {
                                    list += `<p id="city-choice">${result.data.result[i].ville}</p>`

                                }

                                display_choice_where.innerHTML = list
                                linkCity()
                            } else {
                                display_choice_where.innerHTML = `<p id="city">Aucune ville de ce nom là existe</p>`
                            }
                        })
                        .catch((err) => console.log(err))
                }
            })
            display_choice_where.classList.add('show')

        })
        this.linkCity()


        what.addEventListener('focus', () => {
            display_choice_what.classList.add('show')
        })
            what.addEventListener('input', (event) => {
    
                let search = event.target.value
    
                if(search.length > 0) {
                    axios.get("/search/nom=" + search)
                        .then((result) => {
    
                            if (Object.keys(result.data.result).length > 0) {
                                display_choice_what.innerHTML = ''
    
                                let list = ""
    
                                for (let i = 0; i < Object.keys(result.data.result).length; i++) {
                                    list += `<p id="advanced-choice">${result.data.result[i].nom}</p>`
    
                                }
    
                                display_choice_what.innerHTML = list
                                linkAdvanced()
                            } else {
                                display_choice_what.innerHTML = `<p id="city">Aucun nom ou spécilité existe</p>`
                            }
                        })
                        .catch((err) => console.log(err))
                }
            })
        this.linkAdvanced()

        document.getElementById('clear-what').addEventListener('click', () => {
            document.getElementById('what').value = ""
        })
        
        document.getElementById('clear-where').addEventListener('click', () => {
            document.getElementById('where').value = ""
        })
    }
















    linkCity() {
        let city = document.querySelectorAll('#city-choice')

        city.forEach(element => {
            element.addEventListener('click', (event) => {
                where.value = event.target.innerHTML
                display_choice_where.classList.remove('show')
            })
        })
    }

    linkAdvanced() {
        let advanced = document.querySelectorAll('#advanced-choice')

        advanced.forEach(element => {
            element.addEventListener('click', (event) => {
                what.value = event.target.innerHTML
                display_choice_what.classList.remove('show')
            })
        })
    }

}