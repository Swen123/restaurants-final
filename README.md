# Restaurants

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)  [![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg)](http://forthebadge.com)

Application de recherche de restaurants


### Pré-requis

Pour lancer le projet en dev

- Avoir NodeJS installé sur son PC pour installer les dépandences JavaScript [ici](https://nodejs.dev/)
- Avoir composer installé sur son PC pour installer les dépendances PHP [ici](https://getcomposer.org/)
- Avoir Symfony d'installé sur son PC [ici](https://symfony.com/download)
- Avoir un serveur local tel que xampp [ici](https://www.apachefriends.org/fr/index.html) et une base de données nommée 'restos' (voir le fichier sql)

### Installation

Les étapes pour installer le programme....

- Executez la commande ``git clone`` pour cloner le dépôt git
- Executez la commande ``composer install`` pour installer toutes les dépendances PHP.
- Executez la commande ``npm install`` pour installer toutes les dépendances JavaScript

## Démarrage

- Executez la commande ``symfony serve`` pour démarer le server symfony (dev)
- Executez la commande ``npm run build`` pour compiller les assets



## Fabriqué avec

* [Symfony 5](https://symfony.com/) - Framework PHP (Back end)
* [PHPStorm](https://www.jetbrains.com/fr-fr/phpstorm/) - IDE
* [Webpack Encore](https://symfony.com/doc/current/frontend/encore/installation.html) Module Bundler
* [Axios](https://github.com/axios/axios) Requête HTTP


## Auteurs

* **Sweb de Bentzmann** _alias_ [@Swen123](https://gitlab.com/Swen123)

