<?php

namespace App\Entity;

use App\Repository\PlacesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlacesRepository::class)
 */
class Places
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $reservable;

    /**
     * @ORM\Column(type="integer")
     */
    private $liberees;

    /**
     * @ORM\ManyToOne(targetEntity=Restaurants::class, inversedBy="places")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $reservees;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReservable(): ?int
    {
        return $this->reservable;
    }

    public function setReservable(int $reservable): self
    {
        $this->reservable = $reservable;

        return $this;
    }

    public function getLiberees(): ?int
    {
        return $this->liberees;
    }

    public function setLiberees(int $liberees): self
    {
        $this->liberees = $liberees;

        return $this;
    }

    public function getRestaurant(): ?Restaurants
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurants $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getReservees(): ?int
    {
        return $this->reservees;
    }

    public function setReservees(int $reservees): self
    {
        $this->reservees = $reservees;

        return $this;
    }
}
