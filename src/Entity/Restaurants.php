<?php

namespace App\Entity;

use App\Repository\RestaurantsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RestaurantsRepository::class)
 */
class Restaurants
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $addresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $specialite;


    /**
     * @ORM\OneToMany(targetEntity=Reservation::class, mappedBy="restaurant")
     */
    private $reservations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $reservationEnLigne;

    /**
     * @ORM\Column(type="boolean")
     */
    private $commandeEnLigne;

    /**
     * @ORM\Column(type="boolean")
     */
    private $envoiSMS;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nomGerant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephoneGerant;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emailGerant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zip;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="restaurant", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Commandes::class, mappedBy="restaurant")
     */
    private $commandes;

    /**
     * @ORM\OneToMany(targetEntity=Categories::class, mappedBy="restaurant")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity=Places::class, mappedBy="restaurant")
     */
    private $places;

    /**
     * @ORM\OneToMany(targetEntity=Fermetures::class, mappedBy="restaurant")
     */
    private $fermetures;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        $this->commandes = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->places = new ArrayCollection();
        $this->fermetures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAddresse(): ?string
    {
        return $this->addresse;
    }

    public function setAddresse(string $addresse): self
    {
        $this->addresse = $addresse;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getSpecialite(): ?string
    {
        return $this->specialite;
    }

    public function setSpecialite(string $specialite): self
    {
        $this->specialite = $specialite;

        return $this;
    }


    /**
     * @return Collection|Reservation[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setRestaurant($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getRestaurant() === $this) {
                $reservation->setRestaurant(null);
            }
        }

        return $this;
    }

    public function getReservationEnLigne(): ?bool
    {
        return $this->reservationEnLigne;
    }

    public function setReservationEnLigne(bool $reservationEnLigne): self
    {
        $this->reservationEnLigne = $reservationEnLigne;

        return $this;
    }

    public function getCommandeEnLigne(): ?bool
    {
        return $this->commandeEnLigne;
    }

    public function setCommandeEnLigne(bool $commandeEnLigne): self
    {
        $this->commandeEnLigne = $commandeEnLigne;

        return $this;
    }

    public function getEnvoiSMS(): ?bool
    {
        return $this->envoiSMS;
    }

    public function setEnvoiSMS(bool $envoiSMS): self
    {
        $this->envoiSMS = $envoiSMS;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getNomGerant(): ?string
    {
        return $this->nomGerant;
    }

    public function setNomGerant(string $nomGerant): self
    {
        $this->nomGerant = $nomGerant;

        return $this;
    }

    public function getTelephoneGerant(): ?string
    {
        return $this->telephoneGerant;
    }

    public function setTelephoneGerant(?string $telephoneGerant): self
    {
        $this->telephoneGerant = $telephoneGerant;

        return $this;
    }

    public function getEmailGerant(): ?string
    {
        return $this->emailGerant;
    }

    public function setEmailGerant(?string $emailGerant): self
    {
        $this->emailGerant = $emailGerant;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setRestaurant(null);
        }

        // set the owning side of the relation if necessary
        if ($user !== null && $user->getRestaurant() !== $this) {
            $user->setRestaurant($this);
        }

        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Commandes[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commandes $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setRestaurant($this);
        }

        return $this;
    }

    public function removeCommande(Commandes $commande): self
    {
        if ($this->commandes->removeElement($commande)) {
            // set the owning side to null (unless already changed)
            if ($commande->getRestaurant() === $this) {
                $commande->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Categories[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Categories $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->setRestaurant($this);
        }

        return $this;
    }

    public function removeCategory(Categories $category): self
    {
        if ($this->categories->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getRestaurant() === $this) {
                $category->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Places[]
     */
    public function getPlaces(): Collection
    {
        return $this->places;
    }

    public function addPlace(Places $place): self
    {
        if (!$this->places->contains($place)) {
            $this->places[] = $place;
            $place->setRestaurant($this);
        }

        return $this;
    }

    public function removePlace(Places $place): self
    {
        if ($this->places->removeElement($place)) {
            // set the owning side to null (unless already changed)
            if ($place->getRestaurant() === $this) {
                $place->setRestaurant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Fermetures[]
     */
    public function getFermetures(): Collection
    {
        return $this->fermetures;
    }

    public function addFermeture(Fermetures $fermeture): self
    {
        if (!$this->fermetures->contains($fermeture)) {
            $this->fermetures[] = $fermeture;
            $fermeture->setRestaurant($this);
        }

        return $this;
    }

    public function removeFermeture(Fermetures $fermeture): self
    {
        if ($this->fermetures->removeElement($fermeture)) {
            // set the owning side to null (unless already changed)
            if ($fermeture->getRestaurant() === $this) {
                $fermeture->setRestaurant(null);
            }
        }

        return $this;
    }
}
