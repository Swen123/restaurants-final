<?php

namespace App\Entity;

use App\Repository\HoraireRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HoraireRepository::class)
 */
class Horaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $horaire = [];

    /**
     * @ORM\OneToOne(targetEntity=Restaurants::class, cascade={"persist", "remove"})
     */
    private $restaurant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHoraire(): ?array
    {
        return $this->horaire;
    }

    public function setHoraire(array $horaire): self
    {
        $this->horaire = $horaire;

        return $this;
    }

    public function getRestaurant(): ?Restaurants
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurants $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }
}
