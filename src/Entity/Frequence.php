<?php

namespace App\Entity;

use App\Repository\FrequenceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FrequenceRepository::class)
 */
class Frequence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $freq;

    /**
     * @ORM\OneToOne(targetEntity=Restaurants::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFreq(): ?int
    {
        return $this->freq;
    }

    public function setFreq(int $freq): self
    {
        $this->freq = $freq;

        return $this;
    }

    public function getRestaurant(): ?Restaurants
    {
        return $this->restaurant;
    }

    public function setRestaurant(Restaurants $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }
}
