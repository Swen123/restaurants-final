<?php

namespace App\Entity;

use App\Repository\SMSRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SMSRepository::class)
 */
class SMS
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $solde;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $smsUtilise;

    /**
     * @ORM\OneToOne(targetEntity=Restaurants::class, cascade={"persist", "remove"})
     */
    private $restaurant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSolde(): ?int
    {
        return $this->solde;
    }

    public function setSolde(?int $solde): self
    {
        $this->solde = $solde;

        return $this;
    }

    public function getSmsUtilise(): ?int
    {
        return $this->smsUtilise;
    }

    public function setSmsUtilise(?int $smsUtilise): self
    {
        $this->smsUtilise = $smsUtilise;

        return $this;
    }

    public function getRestaurant(): ?Restaurants
    {
        return $this->restaurant;
    }

    public function setRestaurant(?Restaurants $restaurant): self
    {
        $this->restaurant = $restaurant;

        return $this;
    }
}
