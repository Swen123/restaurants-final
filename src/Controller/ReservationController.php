<?php

namespace App\Controller;

use Ramsey\Uuid\Uuid;
use App\Services\sendSMS;
use App\Repository\SMSRepository;
use App\Repository\PlacesRepository;
use App\Repository\HoraireRepository;
use App\Repository\FrequenceRepository;
use App\Repository\FermeturesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ReservationRepository;
use App\Repository\RestaurantsRepository;
use App\Repository\UserReservationRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController; 

class ReservationController extends AbstractController
{

    public function __construct(RequestStack $requestStack,EntityManagerInterface $em,FrequenceRepository $freq,RestaurantsRepository $resto)
    {
        $this->requestStack = $requestStack;
        $this->em = $em;
        $this->freq = $freq;
        $this->resto = $resto;
    }
    
    /**
     * @Route("/reservation/{id}", name="reservation")
     */
    public function index($id, RestaurantsRepository $resto): Response
    {        
        $infos = $resto->getRestaurantInfos($id);

        if($infos['reservation_en_ligne'] == 0){

            $this->addFlash('danger', 'Le restaurant '. $infos['nom'] .'n\'accepte pas les reservations en ligne.');
            return $this->redirectToRoute('home');
        }

        return $this->render('reservation/index.html.twig', [
            'controller_name' => 'ReservationController',
            'id' => $id,
            'infos' => $infos
        ]);
    }





    /**
     * @Route("/get/horaire/{serviceP}/{id}/{dateP}", name="get_horaire")
     */
    public function getHoraire($serviceP, $dateP, FrequenceRepository $freq, $id, HoraireRepository $HoraireRepository,RestaurantsRepository $resto, FermeturesRepository $fermeturesRepository){

    
        $service = ($serviceP == "dejeuner") ? "_m" : "_s";


        $frequence = $freq->findOneByRestoId($id);
        $exist = $HoraireRepository->count($id);
        $fermeture = $fermeturesRepository->fermetureClient($id, $dateP);    
        
        // if($date[0]['date_deb_conge'] != null && $date[0]['date_fin_conge'] != null){
        
        //     if($dateP >= $date[0]['date_deb_conge']->format('Y-m-d') && $dateP <= $date[0]['date_fin_conge']->format('Y-m-d')){
    
        //         return $this->json([
        //             'horaire' => "",
        //             'message' => 'Vous ne pouvez pas réserver le restaurant est en congée jusqu\'au ' . $date[0]['date_fin_conge']->format('d-m-Y') . '.'
        //         ],200);
    
        //     }
        // }

        if($fermeture == 1){
            return $this->json([
                'horaire' => "",
                'message' => 'Vous ne pouvez pas réserver le restaurant est fermé pour congé.'
            ],200); 
        }
        
        if($dateP < date('Y-m-d')){

            return $this->json([
                'horaire' => "",
                'message' => "Vous ne pouvez pas réserver pour une date antérieur."
            ],200);
        }
        
        $horaire = $HoraireRepository->find($exist[0]['id'])->getHoraire();

        $jour = $this->getDay(strtolower(strftime("%w", strtotime($dateP))));


       if($dateP == date('Y-m-d')){
            if($service === "_m" && $horaire[$jour."_f_m"] <= date('H:i')){
                return $this->json([
                    'horaire' => "",
                    'message' => "Le restaurant est actuellement fermé."
                ],200);
            }

            if($service === "_s" && $horaire[$jour."_f_s"] <= date('H:i')){
                return $this->json([
                    'horaire' => "",
                    'message' => "Le restaurant est actuellement fermé."
                ],200);
            }
       }

        if($horaire[$jour."_o".$service] === "" || $horaire[$jour."_f".$service] === ""){
            
            $mORs = ($service == "_m") ? "matin" : "soir";

            return $this->json([
                'horaire' => "",
                'message' => "Le restaurant est fermé le " . $jour . " " . $mORs . "."
            ],200);

        }else{
            $tab = [];

            $ouverture = date('H:i', strtotime($horaire[$jour . "_o" .$service]));        
            $fermeture = date('H:i', strtotime($horaire[$jour . "_f" .$service]));
            $compteur = 1;
      
            $count = abs(date('Hi',strtotime($fermeture)) - date('Hi',strtotime($ouverture))) / 100;        
            $multiplicateur = ($frequence[0]['freq'] == 30) ? 2 : 4;
    
            
            for($i = 0; $i < $count  * $multiplicateur; $i++){
    
                
                $add = strtotime($ouverture . '+ ' . $frequence[0]['freq'] * $i . 'minute');
    
                if(date('H:i', $add) <= $fermeture || date('H:i', $add) >= $ouverture){ 
                    
                   if($dateP == date('Y-m-d')){
                        if(date('H:i', $add) > date('H:i')){
                            $tab[$compteur] = date('H:i', $add);
                            $compteur++;
                        }  
                   }else{
                    $tab[$compteur] = date('H:i', $add);
                    $compteur++;
                   }
                    
                }
               
            }
    
            $tab[$compteur] = $fermeture;
        
    
            return $this->json([
                'horaire' => $tab
            ],200);
        }

   
    }


    /**
     * @Route("/new/reservation/{id}", name="new_reservation")
     */
    public function newReservation($id,Request $request, ReservationRepository $reservation, UserReservationRepository $UserReservation, RestaurantsRepository $restaurant, SMSRepository $sms, PlacesRepository $places){

        
        $place = $places->getPlacesByRestaurantAndDate($id,$request->get('date'))[0];

        

        if($request->get('couvert') != null){
            if($request->get('couvert') < 1){
            
                return $this->json([
                    'type' => 'danger',
                    'message' => 'Vous devez reserver pour une personne minimum.'
                    ],200);
            }

            if($place != null){
                if(intval($place['reservable'] - intval($place['reservees']) > $request->get('couvert'))){

                    $couverts = $place['reservees'] + $request->get('couvert');

                } else {

                    return $this->json([
                        'type' => 'danger',
                        'message' => 'Il n\'y a plus de place disponible pour aujourd\'hui.'
                        ],200);
                }
            }
        }

        $uuid = Uuid::uuid4();

        $reservation->insertReservation(
            $id,
            $request->get('date'),
            $request->get('service'),
            $request->get('horaire'),
            $request->get('couvert'),
            $uuid
        );    

        $places->updatePlacesReservees($place['id'], $couverts);

        $idReservation = $reservation->findByUui($uuid);;

        $UserReservation->insertUserReservation($this->getUser()->getId(),$idReservation);
        
        $roles = $this->getUser()->getRoles();
 
        if($roles[0] === "ROLE_CLIENT"){

            $restaurantInfos = $restaurant->find($id);

            if($restaurantInfos->getEnvoiSMS() === true){



                // $sendSMS->send(
                //     $restaurantInfos->getUser()->getTelephone(),
                //     "Une nouvelle réservation vient d'être enregistrée.",
                //     $restaurantInfos->getNom()
                // );


                $solde = $sms->getSMS($id);
                $newSolde = intval($solde[0]['smsUtilise']) + 1;
                $sms->updateSMSUtilise($id,$newSolde);
            }

        }    

        return $this->json([
            'type' => 'success',
            'message' => "La réservation à bien été prise en compte."
        ],200);

    }

    private function getDay($daynumber){

        switch ($daynumber) {
            case '1':
                return 'lundi';
                break;
            case '2':
                return 'mardi';
                break;
            case '3':  
                return 'mercredi';
                break;
            case '4':  
                return 'jeudi';
                break;
            case '5':
                return 'vendredi';
                break;
            case '6':  
                return 'samedi';
                break;
            case '0':  
                return 'dimanche';
                break;
        }
    }
}


