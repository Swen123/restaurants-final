<?php

namespace App\Controller;

use App\Entity\Banque;
use App\Entity\Places;
use App\Repository\BanqueRepository;
use App\Repository\PlacesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\RestaurantsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PlacesController extends AbstractController{

    /**
     * @Route("/add/places", name="place_add")
     */
    public function addPlace(Request $request, EntityManagerInterface $em, RestaurantsRepository $restaurantRepository){
        

        if($request->get('date') != null || $request->get('id') != null || $request->get('places') != null){

            $places = new Places();

            $places->setDate(\DateTime::createFromFormat('Y-m-d', $request->get('date')));
            $places->setReservable($request->get('places'));
            $places->setLiberees(0);
            $places->setRestaurant($restaurantRepository->find($request->get('id')));

            $em->persist($places);
            $em->flush();

            return $this->json([
                'message' => 'Le nombre de place pour le ' . $places->getDate()->format('d-m-Y') . ' à bien été enregistrée.',
                'type' => 'success'
            ],200);

        } else {
            return $this->json([
                'message' => 'Attention un ou plusieurs champs sont manquants.',
                'type' => 'danger'
            ],200);
        }
    }


    /**
     * @Route("/find/places", name="place_find")
     */
    public function findPlace(PlacesRepository $placesRepository){

       return $this->json($placesRepository->getPlacesByRestaurant($this->getUser()->getRestaurant()));
    }

    /**
     * @Route("/find/places/{id}", name="place_find_id")
     */
    public function getPlacesById(PlacesRepository $placesRepository, $id){

        return $this->json($placesRepository->getPlacesById($id));
     }

    /**
     * @Route("/update/places/{id}", name="update_places")
     */
    public function updatePlaces(Request $request, PlacesRepository $placesRepository, $id){

        if($request->get('reservees') == null || $request->get('liberees') == null || $id == null){

            return $this->json([
                'message' => "Attention certain champs sont vides !",
                'type' => 'danger',
                'code' => 201
            ]);

        } else {
            

           // if() ICI

            $placesRepository->updatePlacesLib($id, $request->get('liberees'));
            $placesRepository->updatePlaces($id, $request->get('reservees'));
            
            return $this->json([
                'message' => "Les places ont bien étés libérées.",
                'type' => 'success',
                'code' => 200
            ]);
        }
        

     }

}