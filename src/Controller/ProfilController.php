<?php

namespace App\Controller;

use App\Entity\Frequence;
use App\Entity\Categories;
use App\Form\FrequenceType;
use App\Form\ProduitMenuType;
use App\Form\ProduitCarteType;
use App\Form\CategoriesMenuType;
use App\Form\CategoriesCarteType;
use App\Repository\SMSRepository;
use App\Repository\UserRepository;
use App\Repository\AchatRepository;
use App\Repository\BanqueRepository;
use App\Repository\HoraireRepository;
use App\Repository\CommandesRepository;
use App\Repository\FrequenceRepository;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ReservationRepository;
use App\Repository\RestaurantsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ProfilController extends AbstractController
{

    public function __construct(){
        $this->HORAIRE = [
            "lundi_o_m" => "",
            "lundi_f_m" => "",
            "mardi_o_m" => "",
            "mardi_f_m" => "",
            "mercredi_o_m" => "",
            "mercredi_f_m" => "",
            "jeudi_o_m" => "",
            "jeudi_f_m" => "",
            "vendredi_o_m" => "",
            "vendredi_f_m" => "",
            "samedi_o_m" => "",
            "samedi_f_m" => "",
            "dimanche_o_m" => "",
            "dimanche_f_m" => "",
            "lundi_o_s" => "",
            "lundi_f_s" => "",
            "mardi_o_s" => "",
            "mardi_f_s" => "",
            "mercredi_o_s" => "",
            "mercredi_f_s" => "",
            "jeudi_o_s" => "",
            "jeudi_f_s" => "",
            "vendredi_o_s" => "",
            "vendredi_f_s" => "",
            "samedi_o_s" => "",
            "samedi_f_s" => "",
            "dimanche_o_s" => "",
            "dimanche_f_s" => ""
        ];
    }

    /**
     * @Route("/mon-compte", name="profil")
     */
    public function index(
        Request $request, 
        UserRepository $userRepository, 
        UserPasswordEncoderInterface $encorder, 
        EntityManagerInterface $em, 
        ReservationRepository $reservationREPO, 
        RestaurantsRepository $restaurantsRepository, 
        CategoriesRepository $categRepo, 
        CommandesRepository $commandesRepository, 
        SMSRepository $SMSRepository, 
        AchatRepository $achatRepository, 
        HoraireRepository $HoraireRepository,
        FrequenceRepository $freq,
        BanqueRepository $banqueRepository
        ): Response
    {

        if($this->getUser() === null){
            return $this->redirectToRoute('home');
        }

        $userId = $this->getUser()->getId();
        $reservations = $reservationREPO->selectAllReservation($this->getUser()->getId());

        $commandes = [];

        if($this->isGranted('ROLE_RESTAURANT')) {

            $commandes = $commandesRepository->selectAllCommande($this->getUser()->getRestaurant()->getId());
        } else {

            $commandes = $commandesRepository->selectAllCommandeClient($this->getUser()->getId());
        }

        if($this->isGranted('ROLE_RESTAURANT')) {

            $getRestaurants = $this->getUser()->getRestaurant();

            $options = [
                'reservation' => $getRestaurants->getreservationEnLigne() === true ? 1 : 0,
                'commande' => $getRestaurants->getcommandeEnLigne()  === true ? 1 : 0,
                'sms' => $getRestaurants->getenvoiSMS()  === true ? 1 : 0
            ];

            $categMenu = $categRepo->findCateg($this->getUser()->getRestaurant()->getId(),"menu");
            $categCarte = $categRepo->findCateg($this->getUser()->getRestaurant()->getId(),"carte");

        } else {
            $options = null;

            $categMenu = null;
            $categCarte = null;
        }
       
        //Formulaire mon compte
        if($request->get('submit') == 1){

            if($this->isGranted('ROLE_RESTAURANT')){

                if($request->get('nom_resto') == null || $request->get('zip') == null || $request->get('email') == null || $request->get('telephone') == null || $request->get('ville') == null){

                    $this->addFlash('danger','Certains champs sont vides');
                
                    return $this->redirectToRoute('profil');
                }

            } else {

                if($request->get('nom') == null || $request->get('prenom') == null || $request->get('email') == null || $request->get('telephone') == null){

                    $this->addFlash('danger','Certains champs sont vides');
                    
                    return $this->redirectToRoute('profil');
    
                }
            }


            if(strlen($request->get('telephone')) !== 10){

                $this->addFlash('danger','Attention le numéro de téléphone doit contenir 10 chiffres.');
                
                return $this->redirectToRoute('profil');
            }
         
            $emailExist = $userRepository->emailExist($request->get('email'),$userId);

            if($emailExist > 0){

                $this->addFlash('danger','Vous ne pouvez pas utiliser ce mail.');
                return $this->redirectToRoute('profil');

            }


            if($this->isGranted('ROLE_RESTAURANT')){

                $file = $request->files->get('photo');
                
                if($file !== null){

                    $extensions_valides=["png","jpg","jpeg"];

                    if (!in_array($file->guessExtension(),$extensions_valides)) {

                        $this->addFlash('error', 'Mauvaise extension de fichier !');
                        return $this->redirectToRoute('profil');

                    } else {

                        $fileName = md5(uniqid()).'.'.$file->guessExtension();
                        $file->move($this->getParameter('upload_directory'),$fileName);


                    }
                } else {
                    $fileName = null;
                }


                $userRepository->updateRestaurantProfile(
                    $request->get('email'), 
                    $request->get('telephone'), 
                    $fileName, 
                    $request->get('nom_resto'), 
                    $request->get('zip'), 
                    $request->get('ville'), 
                    $userId
                );

            } else {

                $userRepository->updateAccount(
                    $request->get('nom'), 
                    $request->get('prenom'), 
                    $request->get('email'), 
                    $request->get('telephone'),  
                    $userId
                );
            }

            $this->addFlash('success','Les modifications ont bien été prise en compte.');
                
            return $this->redirectToRoute('profil');
            
        }

        //Formulaire changement mot de passe
        if($request->get('submitPass') == 1){

            $user = $this->getUser();
            $oldPassword = $request->get('old_password');
           
            if($request->get('password_conf') !== $request->get('new_password')){

                $this->addFlash('danger','Attention la confirmation du mot de passe est différente.');
                
                return $this->redirectToRoute('profil');
            }

            if($request->get('old_password') == null || $request->get('password_conf') == null || $request->get('new_password') == null){

                $this->addFlash('danger','Certains champs sont vides');
                
                return $this->redirectToRoute('profil');
            }

            if($encorder->isPasswordValid($user, $oldPassword)){
                $newPassword = $encorder->encodePassword($user, $request->get('new_password'));

                $user->setPassword($newPassword);

                $em->persist($user);
                $em->flush();

                $this->addFlash('success','Le mot de passe à bien été changé.');
                
                return $this->redirectToRoute('profil');
            } else {
                $this->addFlash('danger','L\'ancien mot de passe est incorrect.');
                
                return $this->redirectToRoute('profil');
            }
        }

        //Formulaire Gérant
        if($request->get('submitGerant') == 1){

            if($request->get('nom_gerant') == null || $request->get('telephone_gerant') == null || $request->get('email_gerant') == null){

                $this->addFlash('danger','Certains champs sont vides');
                
                return $this->redirectToRoute('profil');

            }

            $idRestaurant = $this->getUser()->getRestaurant()->getId();

            //Cherche si l'email est pris par un autre utilisateur et gérant
            $emailTakeByUser = $userRepository->emailExist($request->get('email_gerant'),$idRestaurant);
            $emailTakeByGerant = $restaurantsRepository->emailTakeByGerant($request->get('email_gerant'),$idRestaurant);

            if($emailTakeByUser > 0 || $emailTakeByGerant > 0){

                $this->addFlash('danger','Vous ne pouvez pas utiliser ce mail.');
                return $this->redirectToRoute('profil');

            }

            if(strlen($request->get('telephone_gerant')) !== 10){

                $this->addFlash('danger','Attention le numéro de téléphone doit contenir 10 chiffres.');
                
                return $this->redirectToRoute('profil');
            }

            $userRepository->updateGerant(
                $request->get('nom_gerant'),
                $request->get('telephone_gerant'),
                $request->get('email_gerant'),
                $idRestaurant
            );

            $this->addFlash('success','Les informations du gérant ont bien étés prisent en compte.');
                
            return $this->redirectToRoute('profil');

        }



        if($this->isGranted('ROLE_RESTAURANT')){

            $idRestaurant = $this->getUser()->getRestaurant()->getId();

            $sms = $SMSRepository->getSMS($idRestaurant);
            $achat = $achatRepository->findAll();

            if($sms != null){
                $smsInfo = $sms[0];
            } else {
                $smsInfo = [];
            }
            

            $countOrder = $commandesRepository->getOrderOfTheDay($idRestaurant);
            $countReservation = $reservationREPO->getReservationOfTheDay($idRestaurant);

            $statistic = [
                "order" => $countOrder['commandes'],
                "reservation" => $countReservation['reservations']
            ];

            $exist = $HoraireRepository->count($idRestaurant);

            if($exist == []){
                $horaire = $this->HORAIRE;
            }else{
             
                $horaire = $HoraireRepository->find($exist)->getHoraire();
            }


            //Formulaire fréquence
            $hasFreq = $freq->findOneByRestoId($idRestaurant);
      

            if($hasFreq !== []){
                $frequence = $freq->find($hasFreq[0]['id']);
            }else{
                $restaurant = $restaurantsRepository->find($idRestaurant);
                $frequence = new Frequence();
                $frequence->setRestaurant($restaurant);
            }

            $frequenceType = $this->createForm(FrequenceType::class, $frequence);
           

            //Annalyse la requête
            $frequenceType->handleRequest($request);

            $FreqView = $frequenceType->createView();

            if($frequenceType->isSubmitted() && $frequenceType->isValid()){

                $em->persist($frequence);
                $em->flush();
            

                $this->addFlash('success', 'La fréquence de reservation à bien été mise à jour.');

                return $this->redirectToRoute('profil',[]);
            }

        } 
        

        $banque = $banqueRepository->getBanqueInformations($this->getUser());

        if($banque == null){
            $banque = null;
        } else {
            $banque = $banque[0];
        }

        if($this->isGranted('ROLE_RESTAURANT')){
            return $this->render('profil/index.html.twig', [
                'reservations' => $reservations,
                'options' => $options,
                'categorieMenuForm' => $this->createForm(CategoriesMenuType::class)->createView(),
                'categorieCarteForm' => $this->createForm(CategoriesCarteType::class)->createView(),
                'produitMenuForm' => $this->createForm(ProduitMenuType::class)->createView(),
                'produitCarteForm' => $this->createForm(ProduitCarteType::class)->createView(),
                'categoriesMenu' => $categMenu,
                'categoriesCarte' => $categCarte,
                'commandes' => $commandes,
                'sms' => $smsInfo,
                'achat' => $achat,
                'statistic' => $statistic,
                'idRestaurant' => $idRestaurant,
                'horaire' => $horaire,
                'frequenceType' => $FreqView,
                'banque' => $banque
            ]);
        } else {

            return $this->render('profil/index.html.twig', [
                'reservations' => $reservations,
                'commandes' => $commandes
            ]);
        }
        
    }


    /**
     * @Route("/cancel/reservation/{id}/{uuid}", name="cancel_reservation")
     */
    public function cancel_reservation($id, $uuid, ReservationRepository $reservationREPO): Response{

        $reservationREPO->cancelReservation($id, $uuid);

        $reservations = $reservationREPO->selectAllReservation($this->getUser()->getId());

        return $this->json($reservations,200);
    }

    /**
     * @Route("/set/restaurant/options", name="restaurant_options")
     */
    public function restaurant_Options(Request $request, RestaurantsRepository $restaurantsRepository): Response{
        
        $id = $this->getUser()->getRestaurant()->getId();

        $restaurantsRepository->updateOption(
            boolval(intval($request->get('reservation'))),
            boolval(intval($request->get('commande'))),
            boolval(intval($request->get('sms'))),
            $id
        );

        $options = $restaurantsRepository->getOptions($id);

        return $this->json($options,200);
    }


    /**
     * @Route("/reload/sms", name="reload_sms")
    */
    public function reloadSMS(SMSRepository $SMSRepository){

        $sms = $SMSRepository->getSMS($this->getUser()->getRestaurant()->getId());
    
        return $this->json($sms);
    }
}
