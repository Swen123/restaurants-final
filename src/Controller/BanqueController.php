<?php

namespace App\Controller;

use App\Entity\Banque;
use App\Repository\BanqueRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BanqueController extends AbstractController
{



    /**
     * @Route("/banque", name="banque")
     */
    public function banque(Request $request, EntityManagerInterface $em, BanqueRepository $banqueRepository)
    {
        if($request->get('nomBanque') !== null || $request->get('adresse') !== null || $request->get('zip') !== null || $request->get('ville') !== null || $request->get('iban') !== null || $request->get('bics') !== null){
         
            if(strlen($request->get('iban')) <= 34 && strlen($request->get('iban')) >= 14){

                if(strlen($request->get('bics')) <= 11 && strlen($request->get('bics')) >= 8){
                
                    $banque = new Banque();

                    
                    $banque->setNomBanque($request->get('nomBanque'));
                    $banque->setAdresse($request->get('adresse'));
                    $banque->setZip($request->get('zip'));
                    $banque->setVille($request->get('ville'));
                    $banque->setBICS($request->get('bics'));
                    $banque->setIBAN($request->get('iban'));
                    $banque->setUser($this->getUser());


                    $file = $request->files->get('rib');
                                
                        if($file !== null){

                            $extensions_valides=["pdf","png","jpg","jpeg"];

                            if (!in_array($file->guessExtension(),$extensions_valides)) {

                                $this->addFlash('danger', 'Attention vous ne pouvez pas insérer un fichier avec cette extension !');
                                return $this->redirectToRoute('profil');

                            } else {

                                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                                $file->move($this->getParameter('upload_RIB'),$fileName);

                            }

                            $banque->setRIB($fileName);

                        } else {
                            $banque->setRIB(null);
                        }

                    

                    if(intval($banqueRepository->banqueExist($this->getUser())) === 1){
                        $banqueRepository->updateBanque($banque,$this->getUser()->getId());
                    } else {
                        $em->persist($banque);
                        $em->flush();
                    }
                   

                    $this->addFlash('success', "Les informations de votre banque ont bien étés enregistrées.");
                    return $this->redirectToRoute('profil'); 

                } else {
                    $this->addFlash('danger', "Attention votre code BICS n'est pas valide !");
                    return $this->redirectToRoute('profil'); 
                }

            } else {
                $this->addFlash('danger', "Attention votre numéro IBAN n'est pas valide !");
                return $this->redirectToRoute('profil'); 
            }

        } else {

            $this->addFlash('danger', "Attention certains champs sont manquant !");
            return $this->redirectToRoute('profil'); 
        }

   

        return $this->redirectToRoute('profil'); 
    }

    
}