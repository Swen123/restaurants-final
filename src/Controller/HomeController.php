<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\RestaurantsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(RestaurantsRepository $restaurant): Response
    {

        if($this->isGranted('ROLE_RESTAURANT')) {
            return $this->redirectToRoute('profil');
        }
        
        $city = $restaurant->city();

        $nom = $restaurant->nomRestaurant();
        $specialite = $restaurant->specialiteRestaurant();

      //  dd($specialiteRestaurant);
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'city' => $city,
            'nom' => $nom,
            'specialite' => $specialite
        ]);
    }


    /**
     * @Route("/search/city={value}", name="city_search")
     */
    public function city($value,RestaurantsRepository $restaurant){

        $city = $restaurant->searchCity($value);

        return $this->json([
            'result' => $city
        ],200);
    }

    /**
     * @Route("/search/nom={value}", name="nom_search")
     */
    public function nom($value,RestaurantsRepository $restaurant){

        $result = $restaurant->searchNom($value);

        return $this->json([
            'result' => $result
        ],200);
    }

}
