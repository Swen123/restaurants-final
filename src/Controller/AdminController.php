<?php

namespace App\Controller;

use App\Entity\Frequence;
use App\Form\FrequenceType;
use App\Repository\FermeturesRepository;
use App\Repository\HoraireRepository;
use App\Repository\FrequenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\RestaurantsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{



    /**
     * @Route("/horaire/{id}", name="upd_horaire")
     */
    public function updHoraire(Request $request, $id, HoraireRepository $HoraireRepository){

        $HORAIRE = [
            "lundi_o_m" => $request->get('lundi_o_m'),
            "lundi_f_m" => $request->get('lundi_f_m'),
            "mardi_o_m" => $request->get('mardi_o_m'),
            "mardi_f_m" => $request->get('mardi_f_m'),
            "mercredi_o_m" => $request->get('mercredi_o_m'),
            "mercredi_f_m" => $request->get('mercredi_f_m'),
            "jeudi_o_m" => $request->get('jeudi_o_m'),
            "jeudi_f_m" => $request->get('jeudi_f_m'),
            "vendredi_o_m" => $request->get('vendredi_o_m'),
            "vendredi_f_m" => $request->get('vendredi_f_m'),
            "samedi_o_m" => $request->get('samedi_o_m'),
            "samedi_f_m" => $request->get('samedi_f_m'),
            "dimanche_o_m" => $request->get('dimanche_o_m'),
            "dimanche_f_m" => $request->get('dimanche_f_m'),
            "lundi_o_s" => $request->get('lundi_o_s'),
            "lundi_f_s" => $request->get('lundi_f_s'),
            "mardi_o_s" => $request->get('mardi_o_s'),
            "mardi_f_s" => $request->get('mardi_f_s'),
            "mercredi_o_s" => $request->get('mercredi_o_s'),
            "mercredi_f_s" => $request->get('mercredi_f_s'),
            "jeudi_o_s" => $request->get('jeudi_o_s'),
            "jeudi_f_s" => $request->get('jeudi_f_s'),
            "vendredi_o_s" => $request->get('vendredi_o_s'),
            "vendredi_f_s" => $request->get('vendredi_f_s'),
            "samedi_o_s" => $request->get('samedi_o_s'),
            "samedi_f_s" => $request->get('samedi_f_s'),
            "dimanche_o_s" => $request->get('dimanche_o_s'),
            "dimanche_f_s" => $request->get('dimanche_f_s')
        ];

        $exist = $HoraireRepository->count($id);

        if($exist == []){

            $HoraireRepository->insertHoraire(json_encode($HORAIRE),$id);
            
        }else{

            $HoraireRepository->updateHoraire(json_encode($HORAIRE),$id);
        }

        
        $this->addFlash('success', 'Horaires sauvegardés.');

        return $this->redirectToRoute('profil',[]);

    }    


    /**
     * @Route("/conge/add", name="conge")
     */
    public function setConge(Request $request,FermeturesRepository $fermeturesRepository){

        if($request->get('deb_conge') !== null && $request->get('fin_conge') !== null){
        
            $debut =  $request->get('deb_conge');
            $fin = $request->get('fin_conge');

            if($debut > $fin){

                return $this->json([
                    'message' => 'La date de fin de congé ne peu pas être avant le début des congés.',
                    'type' => 'danger'
                ]);
            }

            $fermeturesRepository->insertFermeture(
                $request->get('deb_conge'),
                $request->get('fin_conge'),
                $this->getUser()->getRestaurant()->getId()
            );

            return $this->json([
                'message' => 'Les congés ont bien été sauvegardés.',
                'type' => 'success'
            ]);
        }

    }   

    /**
     * @Route("/find/fermeture", name="fermeture_find")
     */
    public function findFermeture(FermeturesRepository $fermeturesRepository){

        return $this->json($fermeturesRepository->findFermetureByRestaurant($this->getUser()->getRestaurant()));

    }



    /**
     * @Route("/conge/delete/{id}", name="conge_delete")
     */
    public function deleteConge($id, FermeturesRepository $fermeturesRepository){


        if($id != null){
            $fermeturesRepository->deleteFermeture($id);

            return $this->json([
                'message' => "La période de fermeture selectionné a bien été supprimé.",
                'type' => 'success'
            ]);

        } else {
            return $this->json([
                'message' => "Vous devez avoir un id pour effectuer cette action.",
                'type' => 'danger'
            ]);
        }
    }
}