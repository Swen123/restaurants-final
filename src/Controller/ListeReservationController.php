<?php

namespace App\Controller;

use App\Repository\ReservationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ListeReservationController extends AbstractController
{
    /**
     * @Route("/liste/reservation/{id}", name="liste_reservation")
     */
    public function index($id,ReservationRepository $reservation,PaginatorInterface $paginator,Request $request): Response
    {

        
        $pagination = $paginator->paginate(
            $reservation->findAllReservationById($id),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('liste_reservation/index.html.twig', [
            'controller_name' => 'ListeReservationController',
            'reservations' => $pagination,
            'id' => $id
        ]);
    }
}
