<?php

namespace App\Controller;

use App\Entity\Achat;
use Stripe\Stripe;
use Stripe\Checkout\Session;
use App\Repository\SMSRepository;
use App\Repository\AchatRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\SMS;

class StripeController extends AbstractController
{

    /**
     * @Route("/payment", name="payment")
     */
    public function payment()
    {
        return $this->render('payment/stripe.html.twig', []);
    }

    /**
     * @Route("/create-checkout-session", name="checkout")
     */
    public function checkout(Request $request, AchatRepository $achat)
    {

      $idProduct = $request->get('idProd');

      $product = $achat->find($idProduct);
      

        Stripe::setApiKey('sk_test_51JSdTyKqYQMphBXlwu7fAIQ8Xh2VmY1WQD4IzfSi1TWHNRvYbA1Mpjcmxw8UeipUnjN7udqLSS1WJiKl3znGUKHm00OoXGaKrD');

        $session = Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
              'price_data' => [
                'currency' => 'eur',
                'product_data' => [
                  'name' => $product->getQuantite() . ' SMS',
                ],
                'unit_amount' => $product->getPrix() * 100,
              ],
              'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => $this->generateUrl('checkout-success', [
              'qte' => $product->getQuantite()
            ], UrlGeneratorInterface::ABSOLUTE_URL),
            'cancel_url' => $this->generateUrl('checkout-cancel', [], UrlGeneratorInterface::ABSOLUTE_URL)
          ]);

        return $this->redirect($session->url, 303);
    }


    /**
     * @Route("/checkout-success", name="checkout-success")
     */
    public function success(Request $request, SMSRepository $sms)
    {

      $qteAchete = intval($request->get('qte'));
      $id = $this->getUser()->getRestaurant()->getId();
      $solde = $sms->getSMS($id);



      if($solde != null){

        $newSolde = $qteAchete + intval($solde[0]['solde']);
        $sms->updateSolde($id, $newSolde);

      } else {
        $sms->insertSolde($id, $qteAchete);
      }

        return $this->redirectToRoute('profil');  
    }

    /**
     * @Route("/checkout-cancel", name="checkout-cancel")
     */
    public function cancel()
    {
        return $this->redirectToRoute('profil'); 
    }


    /**
     * @Route("/sms/restant", name="sms_restant")
     */
    public function getSMSRestant(SMSRepository $sms){

      return $this->json($sms->getSMS($this->getUser()->getRestaurant()));
    }

    
}