<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\RestaurantsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{

    /**
     * @Route("/inscription", name="security_registration")
     */
    public function registration(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encorder, RestaurantsRepository $restaurant, UserRepository $UserRepository): Response
    {
        if($this->getUser() !== null){
            return $this->redirectToRoute('home');
        }


        $user = new User();
        //Créé le formulaire "RegistrationType"
        //Relie les champs du formaulaire aux champs de l'utilisateur
        $form = $this->createForm(RegistrationType::class, $user);

        //Annalyse la requête
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){

            //Encode le mot de passe avec la fonction "Bcrypt" (security.yaml)
            $password_hash = $encorder->encodePassword($user, $user->getPassword());

            //initialise le mot de passe de l'utilisateur avec le hash
            $user->setPrenom($request->get('prenom'));
            $user->setNom($request->get('nom'));
            $user->setPassword($password_hash);
            $user->setUsername($user->getEmail());

            if($request->get('restaurant') != null){
                $user->setRoles(['ROLE_RESTAURANT']);
                $user->setNom("");
                $user->setPrenom("");
                $user->setTelephone($request->get('telephone'));

                if($request->get('nom_resto') == null || $request->get('adresse') == null || $request->get('zip') == null || $request->get('ville') == null || $request->get('telephone') == null){
                    
                    $this->addFlash('danger','Attention certain champs sont manquants.');
                    return $this->redirectToRoute('security_login');
                }

                $em->persist($user);
                $em->flush();
                
                $iduser = $UserRepository->emailById($user->getEmail());

                $restaurant->insertRestaurant(
                    $request->get('nom_resto'),
                    $request->get('adresse'),
                    $request->get('ville'),
                    $request->get('zip'),
                    "Français",
                    $iduser
                );
                
                

            } else {
                $user->setRoles(['ROLE_USER']);
            }
            
          

            return $this->redirectToRoute('security_login');
        }
        
        return $this->render('security/registration.html.twig', [
            'form' => $form->createView()
        ]);
        
     
    }


    /**
     * Fonction de connexion
     *
     * @Route("/Connexion", name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        
        if($this->getUser() !== null){
            return $this->redirectToRoute('home');
        }

        $lastUsername = $authenticationUtils->getLastUsername();
        //Récupère les erreurs
        $errors = $authenticationUtils->getLastAuthenticationError();

        return $this->render('security/index.html.twig', [
            'last_username' => $lastUsername,
            'errors' => $errors
        ]); 
    }


    /**
     * Deconnexion
     *
     * @Route("/deconnexion", name="security_logout")
     */
    public function logout(){}
}