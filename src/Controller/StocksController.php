<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Entity\Categories;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\RestaurantsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StocksController extends AbstractController
{

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }
   
    /**
     * @Route("/ajouter/categorie", name="add_categorie")
     */
    public function addCategories(Request $request, CategoriesRepository $repository, RestaurantsRepository $restosRepository): Response
    {

        $restaurant = $restosRepository->find($this->getUser()->getRestaurant()->getId());
        $type = $request->get('type');

        if($request->get('nom') !== null && $type !== null){

            $exist = $repository->categExist($restaurant->getId(), $request->get('nom'), $type);

            if($exist < 1){

                $categorie = new Categories();

                $categorie->setNom(ucfirst($request->get('nom')));
                $categorie->setType($request->get('type'));
                $categorie->setRestaurant($restaurant);

                $this->em->persist($categorie);
                $this->em->flush();
                
                return $this->json([
                    'message' => 'La catégorie à bien été ajoutée.',
                    'msgtype' => 'success',
                    'categorie_id' => $categorie->getId(),
                    'categorie_nom' => $categorie->getNom()
                ],200);

            } else {
                return $this->json([
                    'message' => 'Attention, la catégorie saisie éxiste déjà.',
                    'msgtype' => 'danger'
                ],200);
            }

        } else {
            return $this->json([
                'message' => 'Attention, il faut saisir un nom pour la catégorie',
                'msgtype' => 'danger'
            ],200);
        }

    }



    /**
     * @Route("/ajouter/produit", name="add_produit")
     */
    public function addProduct(Request $request, CategoriesRepository $categRepository, RestaurantsRepository $restosRepository): Response
    {

        $restaurant = $restosRepository->find($this->getUser()->getRestaurant()->getId());


        if($request->get('nom') !== null && $request->get('prix') !== null && $request->get('categorie') !== null){

           // $exist = $repository->categExist($restaurant->getId(), $request->get('nom'), $type);

            //if($exist < 1){

                $produit = new Produit();

                $produit->setNom(ucfirst($request->get('nom')));
                $produit->setPrix($request->get('prix'));
                $produit->setCategorie($categRepository->find($request->get('categorie')));

                $this->em->persist($produit);
                $this->em->flush();
                
                return $this->json([
                    'message' => 'Le produit à bien été ajouté.',
                    'msgtype' => 'success'
                ],200);

            // } else {
            //     return $this->json([
            //         'message' => 'Attention, la catégorie saisie éxiste déjà.',
            //         'msgtype' => 'danger'
            //     ],200);
            // }

        } else {
            return $this->json([
                'message' => 'Attention, il faut saisir un nom pour la catégorie',
                'msgtype' => 'danger'
            ],200);
        }

    }
}
