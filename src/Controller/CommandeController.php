<?php

namespace App\Controller;

use Exception;
use App\Entity\Commandes;
use App\Repository\CategoriesRepository;
use App\Services\sendSMS;
use App\Repository\SMSRepository;
use App\Repository\HoraireRepository;
use App\Repository\CommandesRepository;
use App\Repository\FrequenceRepository;
use App\Repository\FermeturesRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\RestaurantsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController; 

class CommandeController extends AbstractController
{
    
    /**
     * @Route("/commande/{id}", name="commande")
     */
    public function index($id,RestaurantsRepository $resto): Response
    {   

        $infos = $resto->getRestaurantInfos($id);

        if($infos['reservation_en_ligne'] == 0){

            $this->addFlash('danger', 'Le restaurant '. $infos['nom'] .'n\'accepte pas les commandes en ligne.');
            return $this->redirectToRoute('home');
        }

        return $this->render('commande/commande.html.twig', [
            'id' => $id,
            'infos' => $infos
        ]);
    }


    /**
     * @Route("/new/commande/{id}", name="commande_create")
     */
    public function createCommand($id, Request $request, CommandesRepository $commandesRepository,EntityManagerInterface $em, RestaurantsRepository $resto, SMSRepository $sms, sendSMS $sendSMS){

        if($request->get('date') != null && $request->get('service') != null && $request->get('horaire') != null){

            try{

                $commande = new Commandes();

                $commande->setDate(\DateTime::createFromFormat('Y-m-d', $request->get('date')));
                $commande->setHeure($request->get('horaire'));
                $commande->setVente($request->get('service'));
                $commande->setMontant(10);
                $commande->setRestaurant($resto->find($id));
                $commande->setUser($this->getUser());
    
                $em->persist($commande);
                $em->flush();

                $roles = $this->getUser()->getRoles();
 
                if($roles[0] === "ROLE_CLIENT"){
        
                    $restaurantInfos = $resto->find($id);
                
                    if($restaurantInfos->getEnvoiSMS() === true){

                        // $sendSMS->send(
                        //     $restaurantInfos->getUser()->getTelephone(),
                        //     "Une nouvelle commande vient d'être enregistrée.",
                        //     $restaurantInfos->getNom()
                        // );

                        
                        $solde = $sms->getSMS($id);
                        $newSolde = intval($solde[0]['smsUtilise']) + 1;
                        $sms->updateSMSUtilise($id,$newSolde);
                    }
        
                } 

            }catch(Exception $e){
                return $this->json([
                    'type' => 'danger',
                    'message' => $e
                ],200);
            }
         

        }

        return $this->json([
            'type' => 'success',
            'message' => "La commande à bien été passée."
        ],200);
    }

    /**
     * @Route("/horaire/commande/{id}", name="horaire")
     */
    public function horaireCommande(Request $request, $id,HoraireRepository $HoraireRepository,RestaurantsRepository $resto,FrequenceRepository $freq, FermeturesRepository $fermeturesRepository): Response
    {     

        $exist = $HoraireRepository->count($id);   
        $dateC = $request->get('dateC');
        $fermeture = $fermeturesRepository->fermetureClient($id, $dateC); 


        if($fermeture == 1){
            return $this->json([
                'horaireM' => 0,
                'horaireS' => 0,
                'message' => 'Vous ne pouvez pas commander le restaurant est fermé pour congé.'
            ],200); 
        }
        
        if($dateC < date('Y-m-d')){

            return $this->json([
                'error' => true,
                'horaireM' => 0,
                'horaireS' => 0,
                'message' => "Vous ne pouvez pas commander pour une date antérieur."
            ],200);
        }

        $horaire = $HoraireRepository->find($exist)->getHoraire();
        $jour = $this->getDay(strtolower(strftime("%w", strtotime($dateC))));
        $frequence = $freq->findOneByRestoId($id);
        $tabM = [];
        $tabS = [];

        

        for($a = 0; $a < 2; $a++){
            $compteur = 1;
            $mOrs = $a == 1 ? "_s" : "_m";
            $ouverture = date('H:i', strtotime($horaire[$jour . "_o" .$mOrs]));        
            $fermeture = date('H:i', strtotime($horaire[$jour . "_f" .$mOrs]));
        
            $count = abs(date('Hi',strtotime($fermeture)) - date('Hi',strtotime($ouverture))) / 100;        
            $multiplicateur = ($frequence[0]['freq'] == 30) ? 2 : 4;
        
            
            for($i = 0; $i < $count  * $multiplicateur; $i++){
        
                    
                $add = strtotime($ouverture . '+ ' . $frequence[0]['freq'] * $i . 'minute');
        
                if(date('H:i', $add) < $fermeture || date('H:i', $add) >= $ouverture){ 
                        
                    if($dateC == date('Y-m-d')){
                        if(date('H:i', $add) > date('H:i')){

                            if($mOrs === "_m"){

                                $tabM[$compteur] = date('H:i', $add);
                                $compteur++;

                            } else {

                                $tabS[$compteur] = date('H:i', $add);
                                $compteur++;
                            }
                           
                        }  
                    }else{

                        
                        if($mOrs === "_m"){

                            $tabM[$compteur] = date('H:i', $add);
                            $compteur++;

                        } else {

                            $tabS[$compteur] = date('H:i', $add);
                            $compteur++;
                        }
                    
                    }
                        
                }
                
            }

            // if($mOrs === "_m"){

            //     $tabM[$compteur] = $fermeture;

            // } else {

            //     $tabS[$compteur] = $fermeture;
            // }
        }
    
        return $this->json([
            'horaireM' => $tabM,
            'horaireS' => $tabS
        ],200);
    }


    /**
    * @Route("/get/product/{id}", name="get_product")
    */
    public function getProduct($id, ProduitRepository $produitRepository, CategoriesRepository $categoriesRepository){

        //$menus = $produitRepository->findProductMenu($id, "menu");
        //$carte = $produitRepository->findProductMenu($id, "carte");
        $categoriesMenu = $categoriesRepository->findCateg($id, "menu");
        $categoriesCarte = $categoriesRepository->findCateg($id, "carte");

        return $this->json([
            "categoriesMenu" => $categoriesMenu,
            "categoriesCarte" => $categoriesCarte
        ]);
    }

        /**
    * @Route("/get/product/cat/{id}", name="get_product_cat")
    */
    public function getCat($id, ProduitRepository $produitRepository, CategoriesRepository $categoriesRepository){

        $produits = $produitRepository->findProductMenu($id);

        return $this->json([
            "produits" => $produits,
        ]);
    }



    private function getDay($daynumber){

        switch ($daynumber) {
            case '1':
                return 'lundi';
                break;
            case '2':
                return 'mardi';
                break;
            case '3':  
                return 'mercredi';
                break;
            case '4':  
                return 'jeudi';
                break;
            case '5':
                return 'vendredi';
                break;
            case '6':  
                return 'samedi';
                break;
            case '0':  
                return 'dimanche';
                break;
        }
    }

}
