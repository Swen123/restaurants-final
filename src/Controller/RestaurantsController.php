<?php

namespace App\Controller;

use App\Repository\RestaurantsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RestaurantsController extends AbstractController
{

    private $restaurant;

    public function __construct(RestaurantsRepository $restaurant,PaginatorInterface $paginator)
    {
        $this->restaurant = $restaurant;
        $this->paginator = $paginator;
    }
    /**
     * @Route("/restaurants", name="restaurants")
     */
    public function index(Request $request): Response
    {
        $advanced = $request->get('what');
        $ville = $request->get('where');


        if($advanced === "Tous les restaurants" && $ville !== "Toutes les villes"){

            return $this->redirectToRoute('city_restaurant',[
                'ville' => $ville
            ]);

        } elseif ($ville == "Toutes les villes" && $advanced !== "Tous les restaurants"){

            return $this->redirectToRoute('restaurant',[
                'advanced' => $advanced
            ]);


        } elseif ($advanced === "Tous les restaurants" && $ville === "Toutes les villes"){

           return $this->redirectToRoute('all_restaurant');

        }else{

            return $this->redirectToRoute('villeNomSpe_restaurant',[
                'ville' => $ville,
                'advanced' => $advanced
            ]);
        }
    }


    /**
     * @Route("/tous-les-restaurants", name="all_restaurant")
     */
    public function allRestaurant(Request $request){

        $pagination = $this->paginator->paginate(
            $this->restaurant->findAll(),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('restaurants/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/tous-les-restaurants/{ville}", name="city_restaurant")
     */
    public function restaurantByCity(Request $request,$ville){

        $pagination = $this->paginator->paginate(
            $this->restaurant->cityOnly($ville),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('restaurants/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }


        /**
     * @Route("/restaurants/{advanced}", name="restaurant")
     */
    public function restaurant(Request $request,$advanced){

        $pagination = $this->paginator->paginate(
            $this->restaurant->nomSpeOnly($advanced),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('restaurants/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/restaurants/{ville}/{advanced}", name="villeNomSpe_restaurant")
     */
    public function villeNomSpe(Request $request,$ville,$advanced){

        //dd($ville);
        $pagination = $this->paginator->paginate(
            $this->restaurant->villeNomSpeOnly($ville,$advanced,$advanced),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('restaurants/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
