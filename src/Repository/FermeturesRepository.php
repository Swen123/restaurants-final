<?php

namespace App\Repository;

use App\Entity\Fermetures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Fermetures|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fermetures|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fermetures[]    findAll()
 * @method Fermetures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FermeturesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fermetures::class);
    }


    public function findFermetureByRestaurant($restaurant)
    {
            return $this->createQueryBuilder('f')
            ->select('f.id, f.dateDebut, f.dateFin')
            ->where('f.restaurant = :restaurant')
            ->andWhere('f.dateDebut >= :date')
            ->setParameter('restaurant', $restaurant)
            ->setParameter('date',date('Y-m-d'))
            ->getQuery()
            ->getScalarResult()
        ;
    }

    public function fermetureClient($restaurant,$date)
    {
            return $this->createQueryBuilder('f')
            ->select('count(f.id)')
            ->where('f.restaurant = :restaurant')
            ->andWhere('f.dateDebut >= :date')
            ->orWhere('f.dateFin >= :date')
            ->setParameter('restaurant', $restaurant)
            ->setParameter('date',$date)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function deleteFermeture($id){
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'DELETE FROM fermetures WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id
        ]);
    }
    
    
    public function insertFermeture($deb, $fin, $id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO fermetures (date_debut, date_fin, restaurant_id) VALUES (:date_debut, :date_fin, :restaurant_id)';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'date_debut' => $deb,
            'date_fin' => $fin,
            'restaurant_id' => $id
        ]);
    }

    /*
    public function findOneBySomeField($value): ?Fermetures
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
