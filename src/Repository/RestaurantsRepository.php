<?php

namespace App\Repository;

use App\Entity\Restaurants;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Restaurants|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurants|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurants[]    findAll()
 * @method Restaurants[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Restaurants::class);
    }


    public function city(){
        return $this->createQueryBuilder('r')
            ->select('distinct r.ville')
            ->getQuery()
            ->getScalarResult()
        ;
    }

    public function nomRestaurant(){
        return $this->createQueryBuilder('r')
            ->select('distinct r.nom')
            ->getQuery()
            ->getScalarResult()
        ;
    }

    public function specialiteRestaurant(){
        return $this->createQueryBuilder('r')
            ->select('distinct r.specialite')
            ->getQuery()
            ->getScalarResult()
        ;
    }

    public function searchCity(string $value){

        return $this->createQueryBuilder('r')
            ->select('distinct r.ville')
            ->where('r.ville LIKE :ville')
            ->setParameter('ville',str_replace(" ", "%",'%'.strtolower($value).'%'))
            ->getQuery()
            ->getScalarResult();

    }

    public function searchNom(string $value){

        return $this->createQueryBuilder('r')
            ->select('distinct r.nom')
            ->where('r.nom LIKE :nom')
            ->setParameter('nom',str_replace(" ", "%",'%'.strtolower($value).'%'))
            ->getQuery()
            ->getScalarResult();



    }

    public function allRestos(){


        return $this->createQueryBuilder('r')
            ->select('distinct r.id, r.nom, r.ville, r.addresse, r.specialite')
            ->getQuery();

    }


    public function cityOnly($ville){


        return $this->createQueryBuilder('r')
            ->select('r.id, r.nom, r.ville, r.addresse, r.specialite')
            ->where('r.ville = :ville')
            ->setParameter('ville',$ville)
            ->getQuery();

    }

    public function nomSpeOnly($advanced){


        return $this->createQueryBuilder('r')
            ->select('r.id, r.nom, r.ville, r.addresse, r.specialite')
            ->where('r.nom = :nom')
            ->orWhere('r.specialite = :specialite')
            ->setParameter('nom',$advanced)
            ->setParameter('specialite',$advanced)
            ->getQuery();

    }


    public function villeNomSpeOnly($ville,$specialite,$nom){


        return $this->createQueryBuilder('r')
            ->select('r.id, r.nom, r.ville, r.addresse, r.specialite')
            ->where('r.ville = :ville')
            ->andWhere('r.specialite = :specialite OR r.nom = :nom')
            ->setParameter('ville',$ville)
            ->setParameter('specialite',$specialite)
            ->setParameter('nom',$nom)
            ->getQuery();

    }

    public function setConge($deb, $fin, $id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE restaurants SET date_deb_conge = :deb, date_fin_conge = :fin WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'deb' => $deb,
            'fin' => $fin,
            'id' => $id
        ]);
    }

    public function deleteConge($id){
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE restaurants SET date_deb_conge = null, date_fin_conge = null WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id
        ]);
    }

    public function findConge($id){

        return $this->createQueryBuilder('r')
            ->select('r.date_deb_conge, r.date_fin_conge')
            ->where('r.id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getResult();
    }


    public function insertRestaurant($nom, $addresse, $ville, $zip, $specialite, $user_id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO restaurants (nom, addresse,ville,zip,specialite,user_id) VALUES (:nom, :addresse, :ville, :zip, :specialite, :user_id)';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'nom' => $nom,
            'addresse' => $addresse,
            'ville' => $ville,
            'zip' => $zip,
            'specialite' => $specialite,
            'user_id' => $user_id
        ]);
    }

    

    public function updateOption($reservation, $commande, $sms, $id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE restaurants SET reservation_en_ligne = :reservation, commande_en_ligne = :commande, envoi_sms = :sms WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'reservation' => $reservation,
            'commande' => $commande,
            'sms' => $sms,
            'id' => $id
        ]);
    }


    public function getOptions($id){

        return $this->createQueryBuilder('r')
            ->select('r.reservationEnLigne, r.commandeEnLigne, r.envoiSMS')
            ->where('r.id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getResult();
    }


    public function emailTakeByGerant($email,$id){

        return $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->where('r.emailGerant = :email')
            ->andWhere('r.id <> :id')
            ->setParameter('email', $email)
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function getRestaurantInfos($id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT u.telephone, r.photo, r.addresse, r.zip, r.ville, r.commande_en_ligne, r.envoi_sms, r.reservation_en_ligne 
                FROM user u
                RIGHT JOIN restaurants r ON u.restaurant_id = r.id
                WHERE r.id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id
        ]);

        return $stmt->fetch();
    }
}
