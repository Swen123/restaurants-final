<?php

namespace App\Repository;

use App\Entity\Commandes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commandes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commandes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commandes[]    findAll()
 * @method Commandes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commandes::class);
    }


    public function insertCommande($id, $date, $service, $horaire, $user){

        $conn = $this->getEntityManager()->getConnection();

        $sql = "INSERT INTO commandes (restaurant_id, date_c, service, horaire, user_id) VALUES (:restaurant_id, :dateC, :service, :horaire, :user_id)";
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'dateC' => $date,
            'service' => $service,
            'horaire' => $horaire,
            'restaurant_id' => $id,
            'user_id' => $user
        ]);
    }


    public function selectAllCommande($id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT Commandes.id, Commandes.date_c, Commandes.heure, Commandes.vente, Restaurants.nom, User.nom_client, User.prenom, User.telephone, User.roles
                FROM Commandes 
                JOIN Restaurants ON Commandes.restaurant_id = Restaurants.id
                JOIN User ON Commandes.user_id = User.id
                WHERE Commandes.restaurant_id = :id";

        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id
        ]);

        return $stmt->fetchAll();
    }

    public function selectAllCommandeClient($id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT Commandes.id, Commandes.date_c, Commandes.heure, Commandes.vente, Restaurants.nom, User.nom_client, User.prenom, User.telephone, User.roles
                FROM Commandes 
                JOIN Restaurants ON Commandes.restaurant_id = Restaurants.id
                JOIN User ON Commandes.user_id = User.id
                WHERE User.id = :id";

        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id
        ]);

        return $stmt->fetchAll();
    }


    public function getOrderOfTheDay($id){


        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT COUNT(id) AS commandes
                FROM Commandes 
                WHERE date_c = :dateC AND restaurant_id = :id";

        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id,
            'dateC' => date('Y-m-d')
        ]);

        $result = $stmt->fetchAll();

        return $result[0];

    }
}
