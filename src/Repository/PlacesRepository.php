<?php

namespace App\Repository;

use App\Entity\Places;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Places|null find($id, $lockMode = null, $lockVersion = null)
 * @method Places|null findOneBy(array $criteria, array $orderBy = null)
 * @method Places[]    findAll()
 * @method Places[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlacesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Places::class);
    }


    public function getPlacesByRestaurant($restaurant)
    {
        return $this->createQueryBuilder('p')
            ->select('p.id, p.reservable, p.liberees, p.reservees, p.date')
            ->where('p.restaurant = :restaurant')
            ->andWhere('p.date >= :date')
            ->setParameter('restaurant', $restaurant)
            ->setParameter('date',date('Y-m-d'))
            ->getQuery()
            ->getScalarResult()
        ;
    }

    public function getPlacesById($id)
    {
        return $this->createQueryBuilder('p')
            ->select('p.id, p.reservable, p.liberees, p.reservees, p.date')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getScalarResult()
        ;
    }

    public function updatePlaces($id, $reservees){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE places SET reservees = :reservees WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'reservees' => $reservees,
            'id' => $id   
        ]);
    }

    public function updatePlacesLib($id, $liberees){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE places SET liberees = :liberees WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'liberees' => $liberees,
            'id' => $id   
        ]);
    }

    public function getPlacesByRestaurantAndDate($restaurant, $date)
    {
        return $this->createQueryBuilder('p')
            ->select('p.id, p.reservable, p.liberees, p.reservees, p.date')
            ->where('p.restaurant = :restaurant')
            ->andWhere('p.date = :date')
            ->setParameter('restaurant', $restaurant)
            ->setParameter('date', $date)
            ->getQuery()
            ->getScalarResult()
        ;
    }
}
