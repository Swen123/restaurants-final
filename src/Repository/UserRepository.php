<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Restaurants;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function updateAccount($nom, $prenom, $email, $telephone, $id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE user SET nom = :nom, prenom = :prenom, email = :email, telephone = :telephone WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'nom' => $nom,
            'prenom' => $prenom,
            'email' => $email,
            'telephone' => $telephone,
            'id' => $id
        ]);
    }

    public function updatePassword($password, $id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE user SET password = :password WHERE id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'password' => $password,
            'id' => $id
        ]);
    }

    public function emailExist($email,$id){

        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->where('u.email = :email')
            ->andWhere('u.id <> :id')
            ->setParameter('email', $email)
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function emailById($email){

        return $this->createQueryBuilder('u')
            ->select('u.id')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function updateRestaurantProfile($email, $telephone, $photo, $restaurant, $zip, $ville, $id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE user u
                JOIN restaurants r ON u.restaurant_id = r.id
                SET u.email = :email, u.telephone = :telephone, r.photo = :photo, r.nom = :restaurant, r.zip = :zip, r.ville = :ville 
                WHERE u.id = :id';
                
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'email' => $email,
            'telephone' => $telephone,
            'photo' => $photo,
            'restaurant' => $restaurant,
            'zip' => $zip,
            'ville' => $ville,
            'id' => $id
        ]);
    }


    public function updateGerant($nom, $telephone, $email, $id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE restaurants
                SET nom_gerant = :nom, email_gerant = :email, telephone_gerant = :telephone 
                WHERE id = :id';
                
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'email' => $email,
            'telephone' => $telephone,
            'nom' => $nom,
            'id' => $id
        ]);
    }

}






