<?php

namespace App\Repository;

use App\Entity\UserReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserReservation[]    findAll()
 * @method UserReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserReservation::class);
    }

    
    public function insertUserReservation($user, $reservation){

        $conn = $this->getEntityManager()->getConnection();

        $sql = "INSERT INTO user_reservation (user_id, reservation_id) VALUES (:user_id, :reservation_id)";
        $stmt = $conn->prepare($sql);
        $stmt->execute([
            'user_id' => $user,
            'reservation_id' => $reservation
        ]);
    }

    
}
