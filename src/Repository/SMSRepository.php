<?php

namespace App\Repository;

use App\Entity\SMS;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SMS|null find($id, $lockMode = null, $lockVersion = null)
 * @method SMS|null findOneBy(array $criteria, array $orderBy = null)
 * @method SMS[]    findAll()
 * @method SMS[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SMSRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SMS::class);
    }


    public function getSMS($id)
    {
        return $this->createQueryBuilder('s')
            ->select('s.id, s.solde, s.smsUtilise')
            ->andWhere('s.restaurant = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getScalarResult()
        ;
    }


    public function insertSolde($id, $qte){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO sms (restaurant_id, solde, sms_utilise) VALUES (:restaurant_id, :solde, :sms_utilise)';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'restaurant_id' => $id,
            'solde' => $qte,
            'sms_utilise' => 0
        ]);
    }

    public function updateSolde($id, $qte){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE sms SET solde = :solde WHERE restaurant_id = :restaurant';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'solde' => $qte,
            'restaurant' => $id   
        ]);
    }


    public function updateSMSUtilise($id, $qte){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE sms SET sms_utilise = :sms_utilise WHERE restaurant_id = :restaurant';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'sms_utilise' => $qte,
            'restaurant' => $id   
        ]);
    }

}
