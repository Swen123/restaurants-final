<?php

namespace App\Repository;

use App\Entity\Categories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Categories|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categories|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categories[]    findAll()
 * @method Categories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Categories::class);
    }

   
    public function categExist($id, $nom, $type)
    {
        return $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->where('c.libelle = :nom')
            ->andWhere('c.restaurant = :restaurant')
            ->andWhere('c.type = :type')
            ->setParameter('nom', $nom)
            ->setParameter('restaurant', $id)
            ->setParameter('type', $type)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findCateg($id, $type)
    {
        return $this->createQueryBuilder('c')
            ->select('c.id, c.libelle')
            ->where('c.restaurant = :restaurant')
            ->andWhere('c.type = :type')
            ->setParameter('restaurant', $id)
            ->setParameter('type', $type)
            ->getQuery()
            ->getResult()
        ;
    }    
}
