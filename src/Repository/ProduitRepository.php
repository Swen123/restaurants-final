<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produit::class);
    }


    // public function findProductByCat($id)
    // {
    //     return $this->createQueryBuilder('p')
    //         ->select('p.id, p.nom, p.prix')
    //         ->join()
    //         ->where('p.categorie = :id')
    //         ->setParameter('id', $id)
    //         ->getQuery()
    //         ->getResult()
    //     ;
    // }

    public function findProductMenu($id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT id, nom, prix
                FROM produit 
                WHERE categorie_id = :id";

        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id
        ]);

        return $stmt->fetchAll();
    }

    /*
    public function findOneBySomeField($value): ?Produit
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
