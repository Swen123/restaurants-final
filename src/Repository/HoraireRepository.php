<?php

namespace App\Repository;

use App\Entity\Horaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Horaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Horaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Horaire[]    findAll()
 * @method Horaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HoraireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Horaire::class);
    }
    
    public function count($id)
    {
        return $this->createQueryBuilder('h')
            ->select('h.id')
            ->where('h.restaurant = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
    

    public function insertHoraire($horaire, $resto){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO horaire (restaurant_id, horaire) VALUES (:restaurant, :horaire)';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'restaurant' => $resto,
            'horaire' => $horaire
        ]);
    }

    public function updateHoraire($horaire, $resto){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE horaire SET horaire = :horaire WHERE restaurant_id = :restaurant';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'horaire' => $horaire,
            'restaurant' => $resto   
        ]);
    }
}
