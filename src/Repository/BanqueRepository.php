<?php

namespace App\Repository;

use App\Entity\Banque;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Banque|null find($id, $lockMode = null, $lockVersion = null)
 * @method Banque|null findOneBy(array $criteria, array $orderBy = null)
 * @method Banque[]    findAll()
 * @method Banque[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BanqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Banque::class);
    }

    // /**
    //  * @return Banque[] Returns an array of Banque objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function getBanqueInformations($user)
    {
        return $this->createQueryBuilder('b')
            ->select('b.nomBanque, b.adresse, b.ville, b.zip, b.BICS, b.IBAN')
            ->where('b.user = :val')
            ->setParameter('val', $user)
            ->getQuery()
            ->getResult()
        ;
    }

    public function banqueExist($user)
    {
        return $this->createQueryBuilder('b')
            ->select('count(b.id)')
            ->where('b.user = :val')
            ->setParameter('val', $user)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function updateBanque($banque,$id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'UPDATE banque SET nom_banque = :nom, adresse = :adresse, ville = :ville, zip = :zip, bics = :bics, iban = :iban, rib = :rib WHERE user_id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'nom' => $banque->__toString()['nom'],
            'adresse' => $banque->__toString()['adresse'],
            'ville' => $banque->__toString()['ville'],
            'zip' => $banque->__toString()['zip'],
            'bics' => $banque->__toString()['bics'],
            'iban' => $banque->__toString()['iban'],
            'rib' => $banque->__toString()['rib'],
            'id' => $id
        ]);
    }
    
}
