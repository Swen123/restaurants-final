<?php

namespace App\Repository;

use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, Reservation::class);
        $this->em = $em;
    }

    public function findAllReservationById($id){
        return $this->createQueryBuilder('r')
        ->select('r.id, r.service, r.horaire, r.nombre, r.date')
        ->where('r.restaurant = :id')
        ->setParameter('id', $id)
        ->orderBy('r.date', 'DESC')
        ->getQuery()
    ;
    }


    public function insertReservation($id, $date, $service, $horaire, $couvert, $uuid){

        $conn = $this->getEntityManager()->getConnection();

        $sql = "INSERT INTO reservation (restaurant_id, date, service, horaire, nombre,uuid) VALUES (:restaurant_id, :date, :service, :horaire, :nombre,:uuid)";
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'date' => $date,
            'service' => $service,
            'horaire' => $horaire,
            'nombre' => $couvert,
            'restaurant_id' => $id,
            'uuid' => $uuid
        ]);
    }

    public function findByUui($uuid)
    {
        return $this->createQueryBuilder('r')
            ->select('r.id')
            ->where('r.uuid = :uuid')
            ->setParameter('uuid', $uuid)
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }


    public function selectAllReservation($id){

        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT Reservation.uuid, Reservation.date, Reservation.horaire, Reservation.service, Reservation.nombre, Restaurants.nom, user_reservation.id, user.nom_client, user.prenom, user.telephone
                FROM Reservation 
                JOIN Restaurants ON Reservation.restaurant_id = Restaurants.id
                JOIN user_reservation ON Reservation.id = user_reservation.reservation_id
                JOIN user ON user_reservation.user_id = user.id
                WHERE user.id = :id";

        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id
        ]);

        return $stmt->fetchAll();
    }


    public function cancelReservation($id, $uuid){

        $conn = $this->getEntityManager()->getConnection();

        
        $sql = "DELETE FROM user_reservation WHERE id = :id";
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id
        ]);
        
        $sql = "DELETE FROM reservation WHERE uuid = :uuid";
        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'uuid' => $uuid
        ]);

    }


    public function getReservationOfTheDay($id){


        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT COUNT(id) AS reservations
                FROM reservation 
                WHERE date = :dateC AND restaurant_id = :id";

        $stmt = $conn->prepare($sql);
        $stmt->executeQuery([
            'id' => $id,
            'dateC' => date('Y-m-d')
        ]);

        $result = $stmt->fetchAll();

        return $result[0];

    }
}
