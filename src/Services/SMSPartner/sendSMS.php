<?php

namespace App\Services;

use App\Services\SMSPartnerAPI;

class sendSMS
{
    public function __construct()
    {
        $this->API_KEY = $_ENV['SMS_PARTNER_KEY'];
    }

    public function send($telephone, $message, $sender){
        
        $smspartner = new SMSPartnerAPI(false);
        //check credits
        $result = $smspartner->checkCredits('?apiKey='. $this->API_KEY);


        //send SMS
        $fields = array(
            "apiKey"=> $this->API_KEY,
            "phoneNumbers"=> $telephone,
            "message"=> $message,
            "sender" => $sender,
            "scheduledDeliveryDate"=> date('d-m-Y'),
            "time"=>11,
            "minute"=>0

        );
        $smspartner->sendSms($fields);

        // get delivery
        //$result = $smspartner->checkStatusByNumber('?apiKey=YOUR_API_KEY&messageId=666&phoneNumber=xxxxxxxxxx');
    }
}